package com.ilkom.mrfi.petaumkm;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.adapters.ProdukUsahaAdapter;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.ProdukUsahaModel;
import com.ilkom.mrfi.petaumkm.models.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by muamm on 1/29/2018.
 */

public class KelolaProdukActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    List<ProdukUsahaModel> produkUsahaModelList;
    ProdukUsahaAdapter produkUsahaAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    LinearLayout emptyView, errorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kelola_produk);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        recyclerView = findViewById(R.id.rv_produk);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        produkUsahaModelList = new ArrayList<>();
        produkUsahaAdapter = new ProdukUsahaAdapter(KelolaProdukActivity.this);

        emptyView = findViewById(R.id.emptyView);
        errorView = findViewById(R.id.errorView);

        final UserModel user = SharedPrefManager.getInstance(this).getUser();

        swipeRefreshLayout = findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!isNetworkConnected()){
                    errorView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);

                    swipeRefreshLayout.setRefreshing(false);
                }else {
                    errorView.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    produkUsahaModelList.clear();
                    loadRecyclerViewData();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        if(!isNetworkConnected()){
            errorView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }else{
            errorView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

        loadRecyclerViewData();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void loadRecyclerViewData(){
        final UserModel user = SharedPrefManager.getInstance(this).getUser();
        final ProgressDialog progressDialog = ProgressDialog.show(this, "", "Mengambil data usaha...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_INDEX_USAHA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                            if(obj.getBoolean("success")){
                                JSONArray array = obj.getJSONArray("message");

                                for(int i = 0;i < array.length();i++){
                                    JSONObject o = array.getJSONObject(i);
                                    produkUsahaModelList.add(new ProdukUsahaModel(
                                            o.getInt("id"),
                                            o.getString("nama"),
                                            o.getString("deskripsi")
                                    ));
                                }

                                produkUsahaAdapter.setProdukModels(produkUsahaModelList);
                                recyclerView.setAdapter(produkUsahaAdapter);

                            }else{
                                Toast.makeText(KelolaProdukActivity.this, obj.getString("message"), Toast.LENGTH_LONG).show();
                            }

                            if(produkUsahaAdapter.getItemCount() == 0){
                                recyclerView.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                                errorView.setVisibility(View.GONE);
                            }else{
                                recyclerView.setVisibility(View.VISIBLE);
                                emptyView.setVisibility(View.GONE);
                                errorView.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(KelolaProdukActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", String.valueOf(user.getId()));
                return params;
            }
        };
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Cari Usahamu...");
        searchView.setIconifiedByDefault(false);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                final List<ProdukUsahaModel> filteredModelList = filter(produkUsahaModelList, s);

                produkUsahaAdapter.setFilter(filteredModelList);
                return false;
            }
        });

        return true;
    }

    private List<ProdukUsahaModel> filter(List<ProdukUsahaModel> models, String query) {
        query = query.toLowerCase();final List<ProdukUsahaModel> filteredModelList = new ArrayList<>();
        for (ProdukUsahaModel model : models) {
            final String text = model.getNama().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}

