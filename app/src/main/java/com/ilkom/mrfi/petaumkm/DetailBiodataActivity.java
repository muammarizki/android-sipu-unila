package com.ilkom.mrfi.petaumkm;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.UserModel;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by muamm on 3/13/2018.
 */

public class DetailBiodataActivity extends AppCompatActivity {

    TextView tv_npwp,tv_alamat,tv_ktp,tv_provinsi,tv_kota,tv_kecamatan,tv_desa, tv_nama, tv_email;
    CircleImageView foto_profil;
    SwipeRefreshLayout swipeRefreshLayout;
    CoordinatorLayout coordinatorLayout;
    Snackbar snackbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_biodata);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        tv_nama = findViewById(R.id.tv_nama);
        tv_email = findViewById(R.id.tv_email);
        tv_npwp = findViewById(R.id.tv_npwp);
        tv_alamat = findViewById(R.id.tv_alamat);
        tv_ktp = findViewById(R.id.tv_ktp);
        tv_provinsi = findViewById(R.id.tv_provinsi);
        tv_kota = findViewById(R.id.tv_kota);
        tv_kecamatan = findViewById(R.id.tv_kecamatan);
        tv_desa = findViewById(R.id.tv_desa);
        foto_profil = findViewById(R.id.foto_profil);

        coordinatorLayout = findViewById(R.id.coordinator);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh);

        snackbar = Snackbar
                .make(coordinatorLayout, "Tidak Ada Koneksi", Snackbar.LENGTH_INDEFINITE)
                .setAction("TUTUP", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        snackbar.dismiss();
                    }
                });
        snackbar.show();

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!isNetworkConnected()){
                    snackbar.show();
                    swipeRefreshLayout.setRefreshing(false);
                }else {
                    getBiodata();
                    snackbar.dismiss();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        if(!isNetworkConnected()){
            snackbar.show();
        }else{
            snackbar.dismiss();
        }

        getBiodata();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void getBiodata(){

        final UserModel user = SharedPrefManager.getInstance(this).getUser();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_DETAIL_BIODATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                            if(obj.getBoolean("success")){
                                JSONObject detail = obj.getJSONObject("message");

                                tv_nama.setText(detail.getString("nama"));
                                tv_email.setText(detail.getString("email"));
                                tv_npwp.setText(detail.getString("npwp"));
                                tv_alamat.setText(detail.getString("alamat"));
                                tv_ktp.setText(detail.getString("no_ktp"));
                                tv_provinsi.setText(detail.getString("provinsi"));
                                tv_kota.setText(detail.getString("kabupaten"));
                                tv_kecamatan.setText(detail.getString("kecamatan"));
                                tv_desa.setText(detail.getString("desa"));

                                Picasso.with(getApplicationContext())
                                        .load("http://sipu.capung.tech/api/user/foto-profil/get/" + detail.getString("foto_profil"))
                                        .placeholder(R.drawable.avatar)
                                        .into(foto_profil);

                            }else{
                                finish();
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(getApplicationContext(), TambahBiodataActivity.class));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", String.valueOf(user.getId()));
                return params;
            }
        };

        Volley.newRequestQueue(this).add(stringRequest);
    }

//    private void getEdit(int id_user){
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_DETAIL_BIODATA + id_user,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        try {
//                            JSONObject obj = new JSONObject(response);
//
//                            if(obj.getBoolean("success")){
//                                JSONObject detail = obj.getJSONObject("message");
//
//                                startActivity(new Intent(DetailBiodataActivity.this, EditBiodataActivity.class)
//                                .putExtra("id_biodata", detail.getString("id_biodata")));
//
//                            }else{
//                                finish();
//                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
//                                startActivity(new Intent(getApplicationContext(), TambahBiodataActivity.class));
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
//                    }
//                });
//
//        Volley.newRequestQueue(this).add(stringRequest);
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit, menu);
        MenuItem item = menu.findItem(R.id.menu_edit);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case  R.id.menu_edit:
                startActivity(new Intent(DetailBiodataActivity.this, EditBiodataActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getBiodata();
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
