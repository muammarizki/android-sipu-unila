package com.ilkom.mrfi.petaumkm;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.adapters.UsahaLimitAdapter;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.UsahaLimitModel;
import com.ilkom.mrfi.petaumkm.models.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muamm on 3/17/2018.
 */

public class UsahaActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;

    List<UsahaLimitModel> usahaModels;
    UsahaLimitAdapter usahaLimitAdapter;

    final UserModel user = SharedPrefManager.getInstance(this).getUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("UMKM");
        setContentView(R.layout.activity_kelola_usaha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        recyclerView = findViewById(R.id.rv_usaha);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        usahaLimitAdapter = new UsahaLimitAdapter(UsahaActivity.this);
        usahaModels = new ArrayList<>();

        loadRecyclerViewData();

        swipeRefreshLayout = findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItem();
            }

            void refreshItem() {
                usahaModels.clear();

                loadRecyclerViewData();
                onItemLoad();
            }

            void onItemLoad() {

            }
        });
    }

    private void loadRecyclerViewData(){
        if(usahaModels.size()>0){
            usahaModels.clear();
            usahaLimitAdapter.clearModel();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_GET_USAHA_ALL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                            if(obj.getBoolean("success")){
                                JSONArray array = obj.getJSONArray("message");

                                for(int i = 0;i < array.length();i++){
                                    JSONObject o = array.getJSONObject(i);
                                    usahaModels.add(new UsahaLimitModel(
                                            o.getInt("id"),
                                            o.getString("nama"),
                                            o.getString("kota"),
                                            o.getInt("rating"),
                                            o.getInt("jumlah")
                                    ));
                                }

                                usahaLimitAdapter.setUsahaModels(usahaModels);
                                recyclerView.setAdapter(usahaLimitAdapter);

                            }else{
                                Toast.makeText(UsahaActivity.this, obj.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(UsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Cari UMKM...");
        searchView.setIconifiedByDefault(false);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                final List<UsahaLimitModel> filteredModelList = filter(usahaModels, s);

                usahaLimitAdapter.setFilter(filteredModelList);
                return false;
            }
        });

        return true;
    }

    private List<UsahaLimitModel> filter(List<UsahaLimitModel> models, String query) {
        query = query.toLowerCase();final List<UsahaLimitModel> filteredModelList = new ArrayList<>();
        for (UsahaLimitModel model : models) {
            final String text = model.getNama().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
