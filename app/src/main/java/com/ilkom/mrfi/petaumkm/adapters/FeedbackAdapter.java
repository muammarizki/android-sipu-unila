package com.ilkom.mrfi.petaumkm.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.ListProdukActivity;
import com.ilkom.mrfi.petaumkm.R;
import com.ilkom.mrfi.petaumkm.RatingFeedbackActivity;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.FeedbackModel;
import com.ilkom.mrfi.petaumkm.models.ProdukUsahaModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by muamm on 2/6/2018.
 */

public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.FeedbackViewHolder> {

    private Context context;
    private List<FeedbackModel> feedbackModels;

    public FeedbackAdapter(Context context){
        this.context = context;
        this.feedbackModels = new ArrayList<>();
    }

    public FeedbackAdapter.FeedbackViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.feedback_item, null);
        return new FeedbackAdapter.FeedbackViewHolder(view);
    }

    public void setFeedbackModels(List<FeedbackModel> feedbackModels) {
        this.feedbackModels = feedbackModels;
        notifyDataSetChanged();
    }

    public void clearModel(){
        feedbackModels.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final FeedbackAdapter.FeedbackViewHolder holder, final int position) {
        FeedbackModel usaha = feedbackModels.get(position);

        holder.tv_nama.setText("oleh " + usaha.getNama() + ", " + usaha.getWaktu());
        holder.tv_feedback.setText(usaha.getFeedback());
        holder.rating.setRating(usaha.getRating());
        Picasso.with(context)
                .load("http://sipu.capung.tech/api/user/foto-profil/get/" + usaha.getFoto())
                .placeholder(R.drawable.avatar)
                .into(holder.foto);
    }

    @Override
    public int getItemCount() {
//        return feedbackModels.size();
        return feedbackModels != null ? feedbackModels.size() : 0;
    }


    class FeedbackViewHolder extends RecyclerView.ViewHolder {

        TextView tv_nama, tv_feedback;
        RatingBar rating;
        CircleImageView foto;

        public FeedbackViewHolder(View itemView) {
            super(itemView);

            foto = itemView.findViewById(R.id.foto);
            tv_nama = itemView.findViewById(R.id.tv_nama);
            tv_feedback = itemView.findViewById(R.id.tv_feedback);
            rating = itemView.findViewById(R.id.rating);
        }
    }
}
