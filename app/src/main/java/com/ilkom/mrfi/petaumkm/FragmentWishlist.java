package com.ilkom.mrfi.petaumkm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.adapters.WishlistAdapter;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.UserModel;
import com.ilkom.mrfi.petaumkm.models.WishlistModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by muamm on 12/29/2017.
 */

public class FragmentWishlist extends Fragment {

    CoordinatorLayout coordinatorLayout;
    Snackbar snackbar;
    WishlistAdapter wishlistAdapter;
    RecyclerView rv_wishlist;
    List<WishlistModel> wishlistModel;
    LinearLayout emptyView, errorView;
    SwipeRefreshLayout swipeRefreshLayout;

    UserModel user = SharedPrefManager.getInstance(getContext()).getUser();

    public FragmentWishlist(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.activity_wishlist, container, false);

        if (!SharedPrefManager.getInstance(getActivity()).isLoggedIn()) {
            getActivity().finish();
            startActivity(new Intent(getActivity(), LoginActivity.class));
        }

        coordinatorLayout = rootView.findViewById(R.id.coordinator);
        emptyView = rootView.findViewById(R.id.emptyView);
        errorView = rootView.findViewById(R.id.errorView);
        rv_wishlist = rootView.findViewById(R.id.rv_wishlist);

        int columns = 2;
        rv_wishlist.setLayoutManager(new GridLayoutManager(getContext(), columns));

        wishlistModel = new ArrayList<>();
        wishlistAdapter = new WishlistAdapter(getContext());

        snackbar = Snackbar
                .make(coordinatorLayout, "Tidak Ada Koneksi", Snackbar.LENGTH_INDEFINITE)
                .setAction("TUTUP", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        snackbar.dismiss();
                    }
                });

        swipeRefreshLayout = rootView.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!isNetworkConnected()){
                    errorView.setVisibility(View.VISIBLE);
                    rv_wishlist.setVisibility(View.GONE);

                    snackbar.show();
                    swipeRefreshLayout.setRefreshing(false);
                }else {
                    errorView.setVisibility(View.GONE);
                    rv_wishlist.setVisibility(View.VISIBLE);

                    snackbar.dismiss();
                    wishlistModel.clear();
                    getProdukHarapan();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        if(!isNetworkConnected()){
            errorView.setVisibility(View.VISIBLE);
            rv_wishlist.setVisibility(View.GONE);
            snackbar.show();
        }else {
            snackbar.dismiss();
            errorView.setVisibility(View.GONE);
        }

        return rootView;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void getProdukHarapan(){
        if(wishlistModel.size()>0){
            wishlistModel.clear();
            wishlistAdapter.clearModel();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_INDEX_PRODUK_HARAPAN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length();i++){
                                JSONObject o = array.getJSONObject(i);
                                wishlistModel.add(new WishlistModel(
                                        o.getString("id"),
                                        o.getInt("id_produk"),
                                        o.getString("thumbnail"),
                                        o.getString("nama"),
                                        o.getString("harga"),
                                        o.getInt("pengunjung"),
                                        o.getString("usaha"),
                                        o.getString("kota"),
                                        o.getInt("id_user")
                                ));
                            };

                            wishlistAdapter.setWishlistModels(wishlistModel);
                            rv_wishlist.setAdapter(wishlistAdapter);

                            if(wishlistAdapter.getItemCount() == 0){
                                emptyView.setVisibility(View.VISIBLE);
                                rv_wishlist.setVisibility(View.GONE);
                                errorView.setVisibility(View.GONE);
                            }else{
                                rv_wishlist.setVisibility(View.VISIBLE);
                                emptyView.setVisibility(View.GONE);
                                errorView.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(getContext(), error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", String.valueOf(user.getId()));
                return params;
            }
        };
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(getContext()).add(stringRequest);
    }

    @Override
    public void onStart() {
        super.onStart();

        wishlistModel.clear();
        getProdukHarapan();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setActionBarTitle("Wishlist");
    }
}
