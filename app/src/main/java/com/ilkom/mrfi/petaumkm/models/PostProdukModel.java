package com.ilkom.mrfi.petaumkm.models;

import java.util.List;

/**
 * Created by muamm on 2/14/2018.
 */

public class PostProdukModel {

    List<Spesifikasi> spesifikasi;
    List<FotoProduk> foto_produk;
    Produk produk;

    public List<FotoProduk> getFoto_produk() {
        return foto_produk;
    }

    public void setFoto_produk(List<FotoProduk> foto_produk) {
        this.foto_produk = foto_produk;
    }

    public Produk getProduk() {
        return produk;
    }

    public void setProduk(Produk produk) {
        this.produk = produk;
    }

    public class Produk{
        String nama;
        String harga;
        String deskripsi;
        int id_usaha;

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getHarga() {
            return harga;
        }

        public void setHarga(String harga) {
            this.harga = harga;
        }

        public String getDeskripsi() {
            return deskripsi;
        }

        public void setDeskripsi(String deskripsi) {
            this.deskripsi = deskripsi;
        }

        public int getId_usaha() {
            return id_usaha;
        }

        public void setId_usaha(int id_usaha) {
            this.id_usaha = id_usaha;
        }
    }

    public class Spesifikasi{
        String properti;
        String nilai;

        public String getProperti() {
            return properti;
        }

        public void setProperti(String properti) {
            this.properti = properti;
        }

        public String getNilai() {
            return nilai;
        }

        public void setNilai(String nilai) {
            this.nilai = nilai;
        }
    }

    public class FotoProduk{
        String foto;

        public String getFoto() {
            return foto;
        }

        public void setFoto(String foto) {
            this.foto = foto;
        }
    }

    public List<Spesifikasi> getSpesifikasi() {
        return spesifikasi;
    }

    public void setSpesifikasi(List<Spesifikasi> spesifikasi) {
        this.spesifikasi = spesifikasi;
    }
}
