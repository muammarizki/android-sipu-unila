package com.ilkom.mrfi.petaumkm;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.helpers.HargaTextView;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.helpers.VolleySingleton;
import com.ilkom.mrfi.petaumkm.models.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.DrawableBanner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.views.BannerSlider;

/**
 * Created by muamm on 12/29/2017.
 */

public class DetailProdukActivity extends AppCompatActivity {

    TextView tv_nama, tv_deskripsi, tv_pengunjung, tv_harapan, tv_usaha, tv_feedback, tv_spesifikasi;
    RatingBar rating;
    HargaTextView tv_harga;
    BannerSlider slider;
    int id_produk;
    SwipeRefreshLayout swipeRefreshLayout;
    Button btn_harapan, btn_hapus;
    Snackbar snackbar;
    CoordinatorLayout coordinatorLayout;

    String id_harapan;
    RecyclerView spekView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_produk);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        id_produk = getIntent().getIntExtra("id_produk", id_produk);

        slider = findViewById(R.id.slider);

        tv_nama = findViewById(R.id.tv_nama);
        tv_harga = findViewById(R.id.tv_harga);
        tv_deskripsi = findViewById(R.id.tv_deskripsi);
        tv_pengunjung = findViewById(R.id.tv_pengunjung);
        tv_harapan = findViewById(R.id.tv_harapan);
        tv_usaha = findViewById(R.id.tv_usaha);
        tv_feedback = findViewById(R.id.tv_feedback);
        rating = findViewById(R.id.rating);
        tv_spesifikasi = findViewById(R.id.tv_spesifikasi);

        btn_harapan = findViewById(R.id.btn_harapan);
        btn_hapus = findViewById(R.id.btn_hapus);
        coordinatorLayout = findViewById(R.id.coordinator);

        snackbar = Snackbar
                .make(coordinatorLayout, "Tidak Ada Koneksi", Snackbar.LENGTH_LONG)
                .setAction("TUTUP", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        snackbar.dismiss();
                    }
                });

        swipeRefreshLayout = findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!isNetworkConnected()){
                    snackbar.show();
                    swipeRefreshLayout.setRefreshing(false);
                }else {
                    snackbar.dismiss();
                    refresh();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        if(!isNetworkConnected()){
            snackbar.show();
        }else{
            snackbar.dismiss();
        }

        btn_hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hapusHarapan();
                btn_hapus.setVisibility(View.GONE);
                btn_harapan.setVisibility(View.VISIBLE);
            }
        });

        btn_harapan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postHarapan();
                btn_harapan.setVisibility(View.GONE);
                btn_hapus.setVisibility(View.VISIBLE);
            }
        });

        getDetailProduk();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void hapusHarapan(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_HAPUS_PRODUK_HARAPAN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                            if(obj.getBoolean("success")){
                                Toast.makeText(DetailProdukActivity.this, obj.getString("message"), Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(DetailProdukActivity.this, obj.getString("message"),Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DetailProdukActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_harapan", String.valueOf(id_harapan));
                return params;
            }
        };
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);
    }

    public void refresh() {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    private void postHarapan(){
        final UserModel user = SharedPrefManager.getInstance(this).getUser();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_TAMBAH_PRODUK_HARAPAN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);

                            //if no error in response
                            if (obj.getBoolean("success")) {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_produk", String.valueOf(id_produk));
                params.put("id_user", String.valueOf(user.getId()));
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    public void addField(String fitur, String keterangan){
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.spesifikasi_field, null);

        TextView fiturs = rowView.findViewById(R.id.fitur);
        fiturs.setText(fitur);

        TextView keterangans = rowView.findViewById(R.id.keterangan);
        keterangans.setText(keterangan);

        ViewGroup insert = findViewById(R.id.layout_spek);
        insert.addView(rowView, 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void detailUsaha(int id_usaha){
        final ProgressDialog progressDialog = ProgressDialog.show(this, "Sedang Menyiapkan Detail Usaha", "Tunggu sebentar...", false, false);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_DETAIL_USAHA + id_usaha,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                            if(obj.getBoolean("success")){
                                JSONObject detail = obj.getJSONObject("message");

                                startActivity(new Intent(DetailProdukActivity.this, DetailUsahaActivity.class).putExtra("id", detail.getInt("id_usaha")));

                            }else{
                                Toast.makeText(DetailProdukActivity.this, "Detail usaha tidak ditemukan!",Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(DetailProdukActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);
    }

    private void getDetailProduk(){
        final List<Banner> banners = new ArrayList<>();
        final UserModel user = SharedPrefManager.getInstance(this).getUser();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_DETAIL_PRODUK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                            if(obj.getBoolean("success")){
                                final JSONObject detail = obj.getJSONObject("message");

                                detail.getInt("id");

                                tv_nama.setText(detail.getString("nama"));
                                tv_harga.setText(detail.getString("harga"));
                                tv_deskripsi.setText(detail.getString("deskripsi"));
                                tv_pengunjung.setText("Dilihat : " + String.valueOf(detail.getInt("pengunjung")));
                                tv_usaha.setText(detail.getString("usaha"));
                                tv_usaha.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        try {
                                            startActivity(new Intent(DetailProdukActivity.this, DetailUsahaActivity.class).putExtra("id", detail.getInt("id_usaha")));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });

                                rating.setRating(detail.getInt("rating"));
                                tv_harapan.setText("Disematkan : " + String.valueOf(detail.getInt("harapan")));
                                tv_feedback.setText("(" + String.valueOf(detail.getInt("feedback")) + " Feedback)");
                                tv_spesifikasi.setText("1 pcs " + detail.getString("nama"));

                                if(obj.getBoolean("harapan")){
                                    id_harapan = detail.getString("id_harapan");

                                    btn_harapan.setVisibility(View.GONE);
                                    btn_hapus.setVisibility(View.VISIBLE);
                                }else{
                                    btn_harapan.setVisibility(View.VISIBLE);
                                    btn_hapus.setVisibility(View.GONE);
                                }

                                JSONArray array = detail.getJSONArray("foto");
                                if(array.length() != 0){
                                    for(int i = 0;i < array.length();i++){
                                        JSONObject o = array.getJSONObject(i);
                                        banners.add(new RemoteBanner(
                                                "http://sipu.capung.tech/api/produk/foto-produk/get/" + o.getString("nama")
                                        ));
                                    }

                                    for(int i=0;i < banners.size();i++){
                                        banners.get(i).setScaleType(ImageView.ScaleType.FIT_XY);
                                    }

                                    slider.setBanners(banners);
                                }else{
                                    banners.add(new DrawableBanner(
                                            R.drawable.no_image
                                    ));
                                    slider.setBanners(banners);
                                }

                                JSONArray spek = detail.getJSONArray("spesifikasi");
                                for(int i=0;i < spek.length();i++){
                                    JSONObject j = spek.getJSONObject(i);
                                    addField(j.getString("properti"), j.getString("nilai"));
                                }

                                setTitle(detail.getString("nama"));

                            }else{
                                Toast.makeText(DetailProdukActivity.this, "Detail usaha tidak ditemukan!",Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DetailProdukActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", String.valueOf(user.getId()));
                params.put("id_produk", String.valueOf(id_produk));
                return params;
            }
        };

          Volley.newRequestQueue(this).add(stringRequest);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getDetailProduk();
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
