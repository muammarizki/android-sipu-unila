package com.ilkom.mrfi.petaumkm.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by muamm on 3/15/2018.
 */

public class AsetModel {

    @SerializedName("id")
    private int asetId;

    @SerializedName("aset_bawah")
    private String batasBawahAset;

    @SerializedName("aset_atas")
    private String batasAtasAset;

    @SerializedName("nama")
    private String namaAset;

    public int getAsetId() {
        return asetId;
    }

    public void setAsetId(int asetId) {
        this.asetId = asetId;
    }

    public String getBatasBawahAset() {
        return batasBawahAset;
    }

    public void setBatasBawahAset(String batasBawahAset) {
        this.batasBawahAset = batasBawahAset;
    }

    public String getBatasAtasAset() {
        return batasAtasAset;
    }

    public void setBatasAtasAset(String batasAtasAset) {
        this.batasAtasAset = batasAtasAset;
    }

    public String getNamaAset() {
        return namaAset;
    }

    public void setNamaAset(String namaAset) {
        this.namaAset = namaAset;
    }

    @Override
    public String toString(){
        return batasBawahAset + " s.d " + batasAtasAset;
    }

}
