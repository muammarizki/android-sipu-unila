package com.ilkom.mrfi.petaumkm;


import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v7.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.UserModel;
import com.ilkom.mrfi.petaumkm.helpers.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by muamm on 12/11/2017.
 */

public class LoginActivity extends AppCompatActivity {

    private Button btn_login;
    private TextView tv_daftar;
    private EditText et_username,et_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        if (SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }

        et_username = findViewById(R.id.et_username);
        et_password = findViewById(R.id.et_password);

        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                userLogin();
            }
        });

        tv_daftar = findViewById(R.id.tv_daftar);
        tv_daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });

        if(!isNetworkConnected()){
            Toast.makeText(getApplicationContext(), "Tidak Ada Koneksi", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void userLogin(){
        final String username = et_username.getText().toString();
        final String password = et_password.getText().toString();

        if(TextUtils.isEmpty(username)){
            et_username.setError("Username tidak boleh kosong!");
            et_username.requestFocus();
            return;
        }

        if(TextUtils.isEmpty(password)){
            et_password.setError("Password tidak boleh kosong!");
            et_password.requestFocus();
            return;
        }

        final ProgressDialog progressDialog = ProgressDialog.show(this, "", "Tunggu sebentar...", false, false);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject obj = new JSONObject(response);

                            if (obj.getBoolean("success")) {
                                JSONObject userJson = obj.getJSONObject("message");
                                UserModel user = new UserModel(
                                        userJson.getInt("id"),
                                        userJson.getInt("id_peran"),
                                        userJson.getString("username")
                                );

                                SharedPrefManager.getInstance(getApplicationContext()).userLogin(user);

                                Toast.makeText(getApplicationContext(), "Selamat Datang " + userJson.getString("nama"), Toast.LENGTH_SHORT).show();
                                finish();
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            } else {
                                Toast.makeText(getApplicationContext(), "Username atau Password Salah!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }
        };

        Volley.newRequestQueue(this).add(stringRequest);
    }
}





