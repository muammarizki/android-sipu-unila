package com.ilkom.mrfi.petaumkm;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.adapters.ProdukGridAdapter;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.ProdukGridModel;
import com.ilkom.mrfi.petaumkm.models.UsahaModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by muamm on 1/29/2018.
 */

public class ListProdukActivity extends AppCompatActivity {

    int id_usaha;
    String nama_usaha;
    TextView nama;
    LinearLayout emptyView, errorView;

    SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    List<ProdukGridModel> produkGridModels;
    ProdukGridAdapter produkGridAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_produk);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        id_usaha = getIntent().getIntExtra("id", id_usaha);
        nama_usaha = getIntent().getStringExtra("nama");
        setTitle("Produk " + nama_usaha);
        emptyView = findViewById(R.id.emptyView);

        int numberOfColumns = 2;
        recyclerView = findViewById(R.id.rv_list_produk);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));

        produkGridModels = new ArrayList<>();
        produkGridAdapter = new ProdukGridAdapter(ListProdukActivity.this);

        errorView = findViewById(R.id.errorView);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!isNetworkConnected()){
                    errorView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);

                    swipeRefreshLayout.setRefreshing(false);
                }else {
                    errorView.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    produkGridModels.clear();
                    getProduk();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        if(!isNetworkConnected()){
            errorView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }else{
            errorView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

        getProduk();

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void getProduk(){
        if(produkGridModels.size()>0){
            produkGridModels.clear();
            produkGridAdapter.clearModel();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_INDEX_PRODUK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length();i++){
                                JSONObject o = array.getJSONObject(i);
                                produkGridModels.add(new ProdukGridModel(
                                        o.getInt("id"),
                                        o.getString("nama"),
                                        o.getString("harga"),
                                        o.getInt("pengunjung"),
                                        o.getString("icon_file"),
                                        o.getString("usaha")
                                ));
                            }

                            produkGridAdapter.setProdukModels(produkGridModels);
                            recyclerView.setAdapter(produkGridAdapter);

                            if(produkGridAdapter.getItemCount() == 0){
                                recyclerView.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                                errorView.setVisibility(View.GONE);
                            }else{
                                recyclerView.setVisibility(View.VISIBLE);
                                emptyView.setVisibility(View.GONE);
                                errorView.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ListProdukActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_usaha", String.valueOf(id_usaha));
                return params;
            }
        };
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getProduk();
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.tambah, menu);
        menu.findItem(R.id.menu_tambah).setIcon(R.drawable.ic_add);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Cari Produk...");
        searchView.setIconifiedByDefault(false);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                final List<ProdukGridModel> filteredModelList = filter(produkGridModels, s);

                produkGridAdapter.setFilter(filteredModelList);
                return false;
            }
        });

        return true;
    }

    private List<ProdukGridModel> filter(List<ProdukGridModel> models, String query) {
        query = query.toLowerCase();final List<ProdukGridModel> filteredModelList = new ArrayList<>();
        for (ProdukGridModel model : models) {
            final String text = model.getNama().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.menu_tambah:
                produk();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void produk(){
        Intent produk = new Intent(this, TambahProdukActivity.class);
        produk.putExtra("id", id_usaha);
        produk.putExtra("nama", nama_usaha);
        startActivity(produk);
    }

}
