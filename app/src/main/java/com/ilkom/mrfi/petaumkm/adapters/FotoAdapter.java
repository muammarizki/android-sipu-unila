package com.ilkom.mrfi.petaumkm.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ilkom.mrfi.petaumkm.R;

import java.util.List;

/**
 * Created by muamm on 2/7/2018.
 */

public class FotoAdapter extends RecyclerView.Adapter<FotoAdapter.FotoViewHolder> {
    private Context context;
    private List<String> models;

    public FotoAdapter(Context context, List<String> models){
        this.context = context;
        this.models = models;
    }

    public void setModelItem(String path){
        models.add(path);
        notifyDataSetChanged();
    }

    public List<String> getModels() {
        return models;
    }

    public FotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.list_foto, null);
        return new FotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FotoViewHolder holder, final int position) {

        String usaha = models.get(position);
        holder.foto.setDrawingCacheEnabled(true);
        holder.foto.setImageBitmap(getResizedBitmap(BitmapFactory.decodeFile(usaha), 100));
        holder.hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                models.remove(position);
                notifyDataSetChanged();
            }
        });

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    public int getItemCount() {
        return models != null ? models.size() : 0;
    }

    class FotoViewHolder extends RecyclerView.ViewHolder {

        ImageView foto, hapus;

        public FotoViewHolder(View itemView) {
            super(itemView);

            foto = itemView.findViewById(R.id.foto_produk);
            hapus = itemView.findViewById(R.id.hapus);
        }
    }
}
