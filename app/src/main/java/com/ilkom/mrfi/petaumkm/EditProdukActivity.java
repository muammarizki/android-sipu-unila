package com.ilkom.mrfi.petaumkm;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.ilkom.mrfi.petaumkm.adapters.FotoAdapter;
import com.ilkom.mrfi.petaumkm.adapters.FotoEditAdapter;
import com.ilkom.mrfi.petaumkm.adapters.ProdukBaruAdapter;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.helpers.VolleySingleton;
import com.ilkom.mrfi.petaumkm.models.FotoModel;
import com.ilkom.mrfi.petaumkm.models.PostProdukModel;
import com.ilkom.mrfi.petaumkm.models.ProdukBaruModel;
import com.ilkom.mrfi.petaumkm.models.SpesifikasiModel;
import com.ilkom.mrfi.petaumkm.models.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by muamm on 2/21/2018.
 */

public class EditProdukActivity extends AppCompatActivity{

    Button btn_foto, submit;
    EditText nama, harga, deskripsi, fitur, keterangan;
    int id_produk;
    List<String> modelsFoto;
    List<FotoModel> fotoModels;
    LinearLayout linearLayout;
    RecyclerView recyclerView;
    FotoAdapter fotoAdapter;
    FotoEditAdapter fotoEditAdapter;
    int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 44;
    List<SpesifikasiModel> spesifikasiModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_produk);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent (this, LoginActivity.class));
        }

        id_produk = getIntent().getIntExtra("id_produk", id_produk);

        nama = findViewById(R.id.et_nama);
        deskripsi = findViewById(R.id.et_deskripsi);
        harga = findViewById(R.id.et_harga);

        linearLayout = findViewById(R.id.linearLayout);

//        modelsFoto = new ArrayList<>();
        recyclerView = findViewById(R.id.rv_foto);
//        fotoAdapter = new FotoAdapter(this, modelsFoto);

        fotoModels = new ArrayList<>();;
        fotoEditAdapter = new FotoEditAdapter(EditProdukActivity.this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
//        recyclerView.setAdapter(fotoAdapter);

        submit =  findViewById(R.id.btn_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelect(v);
//                simpanData();
            }
        });

        btn_foto =  findViewById(R.id.btn_foto);
        btn_foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(EditProdukActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(EditProdukActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                } else {
                    if(fotoAdapter.getItemCount() == 5){
                        Toast.makeText(EditProdukActivity.this, "Foto maksimal hanya 5", Toast.LENGTH_LONG).show();
                    }else{
                        Intent intent = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(intent, 99);
                    }
                }
            }
        });
        
        getProduk();
    }

    private void getProduk(){
        final UserModel user = SharedPrefManager.getInstance(this).getUser();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_DETAIL_PRODUK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                            if(obj.getBoolean("success")){
                                final JSONObject detail = obj.getJSONObject("message");

                                detail.getInt("id");
                                nama.setText(detail.getString("nama"));
                                deskripsi.setText(detail.getString("deskripsi"));
                                harga.setText(detail.getString("harga"));

                                JSONArray spek = detail.getJSONArray("spesifikasi");
                                for(int i=0;i < spek.length();i++){
                                    JSONObject j = spek.getJSONObject(i);
                                    addField(j.getString("properti"), j.getString("nilai"));
                                }

                                JSONArray array = detail.getJSONArray("foto");
                                for(int i = 0;i < array.length();i++){
                                    JSONObject o = array.getJSONObject(i);
                                    fotoModels.add(new FotoModel(
                                            o.getString("id"),
                                            o.getString("foto")
                                    ));
                                }

                                fotoEditAdapter.setProdukModels(fotoModels);
                                recyclerView.setAdapter(fotoEditAdapter);

                                setTitle(detail.getString("nama"));

                            }else{
                                Toast.makeText(EditProdukActivity.this, "Detail usaha tidak ditemukan!", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditProdukActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", String.valueOf(user.getId()));
                params.put("id_produk", String.valueOf(id_produk));
                return params;
            }
        };

        Volley.newRequestQueue(this).add(stringRequest);
    }

    public List<PostProdukModel.FotoProduk> getFotoProduk(){
        List<PostProdukModel.FotoProduk> fotoProduks = new ArrayList<>();

        for(String path : fotoAdapter.getModels()){
            PostProdukModel.FotoProduk fotoProduk = new PostProdukModel().new FotoProduk();
            fotoProduk.setFoto(convertToBase64(path));
            fotoProduks.add(fotoProduk);
        }

        return fotoProduks;
    }

    public String convertToBase64(String path) {
        Bitmap bm = BitmapFactory.decodeFile(path);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.NO_WRAP);
    }

    public List<PostProdukModel.Spesifikasi> getSpesifikasi(){
        List<PostProdukModel.Spesifikasi> spesifikasis = new ArrayList<>();

        for(SpesifikasiModel spesifikasiModel : spesifikasiModels){
            PostProdukModel.Spesifikasi spesifikasi = new PostProdukModel().new Spesifikasi();
            spesifikasi.setNilai(spesifikasiModel.getNilai());
            spesifikasi.setProperti(spesifikasiModel.getProperti());
            spesifikasis.add(spesifikasi);
        }

        Log.e("data", new Gson().toJson(spesifikasis));

        return spesifikasis;
    }

    public void onSelect(View v) {
        for(int i=0; i < linearLayout.getChildCount(); i++){
            fitur = linearLayout.getChildAt(i).findViewById(R.id.fitur);
            keterangan = linearLayout.getChildAt(i).findViewById(R.id.keterangan);

            SpesifikasiModel spesifikasiModel = new SpesifikasiModel();
            spesifikasiModel.setProperti(fitur.getText().toString());
            spesifikasiModel.setNilai(keterangan.getText().toString());
            spesifikasiModels.add(spesifikasiModel);
        }

        simpanData(id_produk);
    }

    private void simpanData(int id_produk){
        if(fotoAdapter.getItemCount() == 0){
            Toast.makeText(this, "Foto minimal satu dan maksimal lima!", Toast.LENGTH_SHORT).show();
        }else if(TextUtils.isEmpty(nama.getText().toString())){
            Toast.makeText(this, "Nama tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            nama.requestFocus();
        }else if(TextUtils.isEmpty(harga.getText().toString())){
            Toast.makeText(this, "Harga tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            harga.requestFocus();
        }else if(TextUtils.isEmpty(deskripsi.getText().toString())){
            Toast.makeText(this, "Deskripsi tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            deskripsi.requestFocus();
        }else if(linearLayout.getChildCount() == 0){
            Toast.makeText(this, "Spesifikasi tidak boleh kosong!", Toast.LENGTH_SHORT).show();
        }else{
            final ProgressDialog progressDialog = ProgressDialog.show(this, "Sedang Menyimpan Produk", "Tunggu sebentar...", false, false);

            final PostProdukModel postProdukModel = new PostProdukModel();
            PostProdukModel.Produk produk = postProdukModel.new Produk();
            produk.setNama(nama.getText().toString());
            produk.setHarga(harga.getText().toString());
            produk.setDeskripsi(deskripsi.getText().toString());

            postProdukModel.setProduk(produk);

            postProdukModel.setFoto_produk(getFotoProduk());

            postProdukModel.setSpesifikasi(getSpesifikasi());

            StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_EDIT_PRODUK + id_produk,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                //converting response to json object
                                JSONObject obj = new JSONObject(response);

                                if(obj.getBoolean("success")){
                                    finish();
                                    Toast.makeText(getApplicationContext(), "Produk berhasil ditambahkan", Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(getApplicationContext(), "Produk gagal ditambahkan", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                e.getMessage();
                            }

                            progressDialog.dismiss();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }) {

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {

                    byte[] body = new byte[0];
                    try {
                        String mContent = new Gson().toJson(postProdukModel);
                        Log.e("error", mContent);
                        body = mContent.getBytes("UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        Log.e("error", "Unable to gets bytes from JSON", e.fillInStackTrace());
                    }
                    return body;
                }
            };

            VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
        }


    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            if (requestCode == 99 && resultCode == RESULT_OK
                    && null != data) {


                Uri URI = data.getData();
                String[] FILE = { MediaStore.Images.Media.DATA };


                Cursor cursor = getContentResolver().query(URI,
                        FILE, null, null, null);

                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(FILE[0]);
                String ImageDecode = cursor.getString(columnIndex);
                cursor.close();

                Log.e("Error image", ImageDecode);
                fotoAdapter.setModelItem(ImageDecode);


            }
        } catch (Exception e) {
            Toast.makeText(this, "Please try again", Toast.LENGTH_LONG)
                    .show();
        }
    }

    public void addField(String fitur, String keterangan){
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.spesifikasi_item, null);

        EditText fiturs = rowView.findViewById(R.id.fitur);
        fiturs.setText(fitur);

        EditText keterangans = rowView.findViewById(R.id.keterangan);
        keterangans.setText(keterangan);

        ViewGroup insert = findViewById(R.id.linearLayout);
        insert.addView(rowView, 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    public void onAddField(View v) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.spesifikasi_item, null);
        // Add the new row before the add field button.
        linearLayout.addView(rowView, linearLayout.getChildCount() - 1);
    }

    public void onDelete(View v) {
        linearLayout.removeView((View) v.getParent());
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
