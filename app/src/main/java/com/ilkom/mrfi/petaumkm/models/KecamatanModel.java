package com.ilkom.mrfi.petaumkm.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by muamm on 1/27/2018.
 */

public class KecamatanModel {

    @SerializedName("id")
    private int kecamatanId;

    @SerializedName("id_kabupaten")
    private int kabupatenId;

    @SerializedName("nama")
    private String namaKecamatan;

    public int getKecamatanId() {
        return kecamatanId;
    }

    public void setKecamatanId(int kecamatanId) {
        this.kecamatanId = kecamatanId;
    }

    public int getKabupatenId() {
        return kabupatenId;
    }

    public void setKabupatenId(int kabupatenId) {
        this.kabupatenId = kabupatenId;
    }

    public String getNamaKecamatan() {
        return namaKecamatan;
    }

    public void setNamaKecamatan(String namaKecamatan) {
        this.namaKecamatan = namaKecamatan;
    }

    @Override
    public String toString(){
        return namaKecamatan;
    }
}
