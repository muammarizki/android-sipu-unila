package com.ilkom.mrfi.petaumkm.helpers;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.ilkom.mrfi.petaumkm.LoginActivity;
import com.ilkom.mrfi.petaumkm.models.UserModel;

/**
 * Created by muamm on 12/28/2017.
 */

public class SharedPrefManager {
    //the constants
    private static final String SHARED_PREF_NAME = "petaumkmsharedpref";
    private static final String KEY_ID = "id";
    private static final String KEY_PERAN = "id_peran";
    private static final String KEY_USERNAME = "username";

    private static SharedPrefManager mInstance;
    private static Context mCtx;

    private SharedPrefManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }

    //method to let the user login
    //this method will store the user data in shared preferences
    public static void userLogin(UserModel user) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_ID, user.getId());
        editor.putInt(KEY_PERAN, user.getPeran());
        editor.putString(KEY_USERNAME, user.getUsername());
        editor.apply();
    }

    //this method will checker whether user is already logged in or not
    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USERNAME, null) != null;
    }

    //this method will give the logged in user
    public UserModel getUser() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new UserModel(
                sharedPreferences.getInt(KEY_ID, -1),
                sharedPreferences.getInt(KEY_PERAN, -1),
                sharedPreferences.getString(KEY_USERNAME, null)
        );
    }

    //this method will logout the user
    public void logout() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        mCtx.startActivity(new Intent(mCtx, LoginActivity.class));
    }
}
