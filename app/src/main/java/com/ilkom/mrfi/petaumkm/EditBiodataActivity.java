package com.ilkom.mrfi.petaumkm;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.helpers.VolleySingleton;
import com.ilkom.mrfi.petaumkm.models.DesaModel;
import com.ilkom.mrfi.petaumkm.models.KabupatenModel;
import com.ilkom.mrfi.petaumkm.models.KecamatanModel;
import com.ilkom.mrfi.petaumkm.models.ProvinsiModel;
import com.ilkom.mrfi.petaumkm.models.UserModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by muamm on 2/21/2018.
 */

public class EditBiodataActivity extends AppCompatActivity {

    EditText et_nama, et_email, et_npwp, et_ktp, et_alamat;
    Spinner sp_provinsi, sp_kecamatan, sp_kabupaten, sp_desa;
    CircleImageView foto_profil;
    int id_pelaku;
    Button btn_ganti;
    int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 44;

    String id_desa;

    ArrayAdapter<ProvinsiModel> adapterProvinsi;
    ArrayAdapter<KabupatenModel> adapterKabupaten;
    ArrayAdapter<KecamatanModel> adapterKecamatan;
    ArrayAdapter<DesaModel> adapterDesa;

    private List<ProvinsiModel> provinsiModelList = new ArrayList<>();
    private List<KabupatenModel> kabupatenModelList = new ArrayList<>();
    private List<KecamatanModel> kecamatanModelList = new ArrayList<>();
    private List<DesaModel> desaModelList = new ArrayList<>();
    private JSONObject detailBiodata;
    private boolean firstData = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_biodata);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

//        id_pelaku = getIntent().getIntExtra("id_biodata", id_pelaku);

        et_nama = findViewById(R.id.et_nama);
        et_email = findViewById(R.id.et_email);
        et_npwp = findViewById(R.id.et_npwp);
        et_ktp = findViewById(R.id.et_ktp);
        et_alamat = findViewById(R.id.et_alamat);

        sp_provinsi = findViewById(R.id.sp_provinsi);
        sp_provinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String provinsi = String.valueOf(provinsiModelList.get(position).getProvinsiId());
                getKabupaten(provinsi);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_kabupaten = findViewById(R.id.sp_kabupaten);
        sp_kabupaten.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String kabupaten = String.valueOf(kabupatenModelList.get(position).getKabupatenId());
                getKecamatan(kabupaten);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_kecamatan = findViewById(R.id.sp_kecamatan);
        sp_kecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String kecamatan = String.valueOf(kecamatanModelList.get(position).getKecamatanId());
                getDesa(kecamatan);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_desa = findViewById(R.id.sp_desa);

        sp_desa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                id_desa = String.valueOf(desaModelList.get(position).getDesaId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        foto_profil = findViewById(R.id.foto_profil);

        btn_ganti = findViewById(R.id.btn_ganti);
        btn_ganti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(EditBiodataActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(EditBiodataActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                } else {
                    Intent intent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(intent, 99);
                }
            }
        });

        adapterProvinsi = new ArrayAdapter<>(EditBiodataActivity.this, android.R.layout.simple_spinner_item, provinsiModelList);
        adapterKabupaten = new ArrayAdapter<>(EditBiodataActivity.this, android.R.layout.simple_spinner_item, kabupatenModelList);
        adapterKecamatan = new ArrayAdapter<>(EditBiodataActivity.this, android.R.layout.simple_spinner_item, kecamatanModelList);
        adapterDesa = new ArrayAdapter<>(EditBiodataActivity.this, android.R.layout.simple_spinner_item, desaModelList);

        getBiodata();

    }

    private void getProvinsi(){

        if(provinsiModelList.size() > 0){
            if (kabupatenModelList.size() > 0) {
                if(kecamatanModelList.size() > 0){
                    if(desaModelList.size() > 0){
                        desaModelList.clear();
                        adapterDesa.notifyDataSetChanged();
                    }
                    kecamatanModelList.clear();
                    adapterKecamatan.notifyDataSetChanged();
                }
                kabupatenModelList.clear();
                adapterKabupaten.notifyDataSetChanged();
            }
            provinsiModelList.clear();
            adapterProvinsi.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_PROVINSI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            int defaultPro = 0;
                            for(int i = 0;i < array.length(); i++){
                                ProvinsiModel provinsiModel = new Gson().fromJson(array.getString(i), ProvinsiModel.class);
                                if(provinsiModel.getProvinsiId() == detailBiodata.getInt("id_provinsi") && firstData){
                                    defaultPro = i;
                                }
                                provinsiModelList.add(provinsiModel);
                            }

                            adapterProvinsi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_provinsi.setAdapter(adapterProvinsi);
                            sp_provinsi.setSelection(defaultPro);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditBiodataActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getKabupaten(String provinsi){
        if (kabupatenModelList.size() > 0) {
            if(kecamatanModelList.size() > 0){
                if(desaModelList.size() > 0){
                    desaModelList.clear();
                    adapterDesa.notifyDataSetChanged();
                }
                kecamatanModelList.clear();
                adapterKecamatan.notifyDataSetChanged();
            }
            kabupatenModelList.clear();
            adapterKabupaten.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_KABUPATEN + provinsi,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            int defaultPro = 0;
                            for(int i = 0;i < array.length(); i++){
                                KabupatenModel kabupatenModel = new Gson().fromJson(array.getString(i), KabupatenModel.class);
                                if(kabupatenModel.getKabupatenId() == detailBiodata.getInt("id_kabupaten") && firstData){
                                    defaultPro = i;
                                }
                                kabupatenModelList.add(kabupatenModel);
                            }

                            adapterKabupaten.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_kabupaten.setAdapter(adapterKabupaten);
                            sp_kabupaten.setSelection(defaultPro);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditBiodataActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getKecamatan(String kabupaten){
        if (kecamatanModelList.size() > 0) {
            if(desaModelList.size() > 0){
                desaModelList.clear();
                adapterDesa.notifyDataSetChanged();
            }
            kecamatanModelList.clear();
            adapterKecamatan.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_KECAMATAN + kabupaten,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            int defaultPro = 0;
                            for(int i = 0;i < array.length(); i++){
                                KecamatanModel kecamatanModel = new Gson().fromJson(array.getString(i), KecamatanModel.class);
                                if(kecamatanModel.getKecamatanId() == detailBiodata.getInt("id_kecamatan") && firstData){
                                    defaultPro = i;
                                }
                                kecamatanModelList.add(kecamatanModel);
                            }

                            adapterKecamatan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_kecamatan.setAdapter(adapterKecamatan);
                            sp_kecamatan.setSelection(defaultPro);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditBiodataActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getDesa(String kecamatan){
        if (desaModelList.size() > 0) {
            desaModelList.clear();
            adapterDesa.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_DESA + kecamatan,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            int defaultPro = 0;
                            for(int i = 0;i < array.length(); i++){
                                DesaModel desaModel = new Gson().fromJson(array.getString(i), DesaModel.class);
                                if(desaModel.getDesaId().equalsIgnoreCase(detailBiodata.getString("id_desa")) && firstData){
                                    defaultPro = i;
                                    firstData = false;
                                }
                                desaModelList.add(desaModel);
                            }

                            adapterDesa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_desa.setAdapter(adapterDesa);
                            sp_desa.setSelection(defaultPro);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditBiodataActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    public String convertToBase64(String path) {
        Bitmap bm = getResizedBitmap(BitmapFactory.decodeFile(path), 500);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 60, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 99 && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            String foto = convertToBase64(picturePath);

            updateFotoProfil(foto);

        }
    }

    private void updateFotoProfil(final String foto){
        final UserModel user = SharedPrefManager.getInstance(this).getUser();

        final ProgressDialog progressDialog = ProgressDialog.show(this, "", "Memperbaharui foto profil...", false, false);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_GANTI_FOTO_PROFIL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);

                            //if no error in response
                            if (obj.getBoolean("success")) {

                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                                finish();
//
                            } else {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", String.valueOf(user.getId()));
                params.put("foto", foto);
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void getBiodata(){
        final UserModel user = SharedPrefManager.getInstance(this).getUser();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_DETAIL_BIODATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                            if(obj.getBoolean("success")){
                                detailBiodata = obj.getJSONObject("message");

                                et_nama.setText(detailBiodata.getString("nama"));
                                et_email.setText(detailBiodata.getString("email"));
                                et_ktp.setText(detailBiodata.getString("no_ktp"));
                                et_npwp.setText(detailBiodata.getString("npwp"));
                                et_alamat.setText(detailBiodata.getString("alamat"));

                                id_pelaku = Integer.parseInt(detailBiodata.getString("id_pelaku"));

                                Picasso.with(getApplicationContext())
                                        .load("http://sipu.capung.tech/api/user/foto-profil/get/" + detailBiodata.getString("foto_profil"))
                                        .placeholder(R.drawable.avatar)
                                        .into(foto_profil);

                                getProvinsi();

                            }else{
                                finish();
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(getApplicationContext(), TambahBiodataActivity.class));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", String.valueOf(user.getId()));
                return params;
            }
        };

        Volley.newRequestQueue(this).add(stringRequest);
    }

    private void updateBiodata(){
        final UserModel user = SharedPrefManager.getInstance(this).getUser();

        final String nama = et_nama.getText().toString().trim();
        final String email = et_email.getText().toString().trim();
        final String npwp = et_npwp.getText().toString().trim();
        final String ktp = et_ktp.getText().toString().trim();
        final String alamat = et_alamat.getText().toString().trim();
        final String id_user = String.valueOf(user.getId());

        final ProgressDialog progressDialog = ProgressDialog.show(this, "", "Memperbaharui data diri...", false, false);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_EDIT_BIODATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);

                            //if no error in response
                            if (obj.getBoolean("success")) {

                                finish();
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_LONG).show();

                            } else {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_pelaku", String.valueOf(id_pelaku));
                params.put("id_user", String.valueOf(user.getId()));
                params.put("nama", nama);
                params.put("email", email);
                params.put("npwp", npwp);
                params.put("no_ktp", ktp);
                params.put("alamat", alamat);
                params.put("id_desa", id_desa);
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.simpan, menu);
        MenuItem item = menu.findItem(R.id.menu_simpan);
        item.setIcon(R.drawable.ic_simpan).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case  R.id.menu_simpan:
                updateBiodata();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
