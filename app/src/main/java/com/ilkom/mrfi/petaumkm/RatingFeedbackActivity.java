package com.ilkom.mrfi.petaumkm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.adapters.FeedbackAdapter;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.FeedbackModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muamm on 12/29/2017.
 */

public class RatingFeedbackActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    List<FeedbackModel> feedbackModels;
    int id_usaha;
    FeedbackAdapter feedbackAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating_feedback);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        id_usaha = getIntent().getIntExtra("id", id_usaha);

        recyclerView = findViewById(R.id.rv_feedback);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        feedbackAdapter = new FeedbackAdapter(RatingFeedbackActivity.this);
        feedbackModels = new ArrayList<>();

        getFeedback(id_usaha);
    }

    private void getFeedback(int id){
        if(feedbackModels.size()>0){
            feedbackModels.clear();
            feedbackAdapter.clearModel();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_GET_FEEDBACK_USAHA_ALL + id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length();i++){
                                JSONObject o = array.getJSONObject(i);
                                feedbackModels.add(new FeedbackModel(
                                        o.getString("id"),
                                        o.getString("nama"),
                                        o.getInt("rating"),
                                        o.getString("feedback"),
                                        o.getString("waktu"),
                                        o.getString("foto_profil")
                                ));
                            }

                            feedbackAdapter.setFeedbackModels(feedbackModels);
                            recyclerView.setAdapter(feedbackAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(RatingFeedbackActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
