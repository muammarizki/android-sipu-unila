package com.ilkom.mrfi.petaumkm.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.DetailProdukActivity;
import com.ilkom.mrfi.petaumkm.R;
import com.ilkom.mrfi.petaumkm.helpers.HargaTextView;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.helpers.VolleySingleton;
import com.ilkom.mrfi.petaumkm.models.ProdukBaruModel;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by muamm on 2/28/2018.
 */

public class ProdukIndexGridAdapter extends RecyclerView.Adapter<ProdukIndexGridAdapter.ProdukViewHolder>{
    private Context context;
    private List<ProdukBaruModel> models;

    public ProdukIndexGridAdapter(Context context){
        this.context = context;
        this.models = new ArrayList<>();
    }

    public ProdukIndexGridAdapter.ProdukViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_produk_all, null);
        return new ProdukIndexGridAdapter.ProdukViewHolder(view);
    }

    public void setProdukModels(List<ProdukBaruModel> produkModels) {
        this.models = produkModels;
        notifyDataSetChanged();
    }

    public void setFilter(List<ProdukBaruModel> produkBaruModels) {
        models = new ArrayList<>();
        models.addAll(produkBaruModels);
        notifyDataSetChanged();
    }

    public void clearModel(){
        notifyDataSetChanged();
        models.clear();
    }

    @Override
    public void onBindViewHolder(final ProdukIndexGridAdapter.ProdukViewHolder holder, final int position) {
        ProdukBaruModel produk = models.get(position);

        holder.tv_harga.setText(produk.getHarga());
        holder.tv_umkm.setText(produk.getUmkm());
        holder.tv_kota.setText(produk.getKota());
        holder.tv_pengunjung.setText(String.valueOf(produk.getPengunjung()));
        holder.tv_nama.setText(produk.getNama());
        Picasso.with(context)
                .load("http://sipu.capung.tech/api/produk/thumb/get/" + produk.getThumbnail())
                .into(holder.thumbnail);

        holder.cv_produk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postPengunjung(models.get(position).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    class ProdukViewHolder extends RecyclerView.ViewHolder {

        TextView tv_nama, tv_pengunjung, tv_umkm, tv_kota;
        HargaTextView tv_harga;
        CardView cv_produk;
        ImageView thumbnail;

        public ProdukViewHolder(View itemView) {
            super(itemView);

            tv_nama = itemView.findViewById(R.id.tv_nama);
            tv_pengunjung = itemView.findViewById(R.id.pengunjung);
            cv_produk = itemView.findViewById(R.id.cv_produk);
            tv_kota = itemView.findViewById(R.id.lokasi_umkm);
            tv_umkm = itemView.findViewById(R.id.umkm);
            tv_harga = itemView.findViewById(R.id.tv_harga);
            thumbnail = itemView.findViewById(R.id.thumbnail);
        }
    }

    private void postPengunjung(final int id_produk){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_TAMBAH_PENGUNJUNG,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);

                            //if no error in response
                            if (obj.getBoolean("success")) {

                                context.startActivity(new Intent(context, DetailProdukActivity.class).putExtra("id_produk", id_produk));

                            } else {
                                Toast.makeText(context, obj.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_produk", String.valueOf(id_produk));
                return params;
            }
        };

        Volley.newRequestQueue(context).add(stringRequest);
    }
}