package com.ilkom.mrfi.petaumkm.models;

/**
 * Created by muamm on 1/21/2018.
 */

public class UsahaModel {

     int id;
     String nama;
     String deskripsi;
     String alamat;
     int noTelpon;
     int tahunData;
     double latitude;
     double longitude;
     int peglk;
     int pegpr;
     int idPelaku;
     int idJenisUsaha;
     int idOmset;
     int idIzinUsaha;
     String noIzinUsaha;
     int idDesa;

    public UsahaModel(int id, String nama, String deskripsi, String alamat, int noTelpon, int tahunData, double latitude,
                      double longitude, int idPelaku, int idJenisUsaha, int idOmset, int idIzinUsaha, int peglk, int pegpr,
                      String noIzinUsaha, int idDesa){
        this.id = id;
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.alamat = alamat;
        this.noTelpon = noTelpon;
        this.tahunData = tahunData;
        this.latitude = latitude;
        this.longitude = longitude;
        this.idPelaku = idPelaku;
        this.idJenisUsaha = idJenisUsaha;
        this.idOmset = idOmset;
        this.idIzinUsaha = idIzinUsaha;
        this.peglk = peglk;
        this.pegpr = pegpr;
        this.noIzinUsaha = noIzinUsaha;
        this.idDesa = idDesa;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public int getNoTelpon() {
        return noTelpon;
    }

    public void setNoTelpon(int noTelpon) {
        this.noTelpon = noTelpon;
    }

    public int getTahunData() {
        return tahunData;
    }

    public void setTahunData(int tahunData) {
        this.tahunData = tahunData;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getIdPelaku() {
        return idPelaku;
    }

    public void setIdPelaku(int idPelaku) {
        this.idPelaku = idPelaku;
    }

    public int getIdJenisUsaha() {
        return idJenisUsaha;
    }

    public void setIdJenisUsaha(int idJenisUsaha) {
        this.idJenisUsaha = idJenisUsaha;
    }

    public int getIdOmset() {
        return idOmset;
    }

    public void setIdOmset(int idOmset) {
        this.idOmset = idOmset;
    }

    public int getIdIzinUsaha() {
        return idIzinUsaha;
    }

    public void setIdIzinUsaha(int idIzinUsaha) {
        this.idIzinUsaha = idIzinUsaha;
    }
}
