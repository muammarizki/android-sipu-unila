package com.ilkom.mrfi.petaumkm;

import android.Manifest;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.ilkom.mrfi.petaumkm.adapters.FotoAdapter;
import com.ilkom.mrfi.petaumkm.adapters.SpesifikasiAdapter;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.helpers.VolleySingleton;
import com.ilkom.mrfi.petaumkm.models.PostProdukModel;
import com.ilkom.mrfi.petaumkm.models.SpesifikasiModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by muamm on 1/29/2018.
 */

public class TambahProdukActivity extends AppCompatActivity {

    int id_usaha;
    String nama_usaha;
    EditText nama,deskripsi,harga, fitur, keterangan;
    Button submit, btn_foto, btn_spesifikasi;
    LinearLayout linearLayout;
    FotoAdapter fotoAdapter;
    SpesifikasiAdapter spesifikasiAdapter;
    List<String> modelsFoto;
    List<SpesifikasiModel> spesifikasiModels;
    List<PostProdukModel> postProdukModels;
    RecyclerView recyclerView, rv_spek;
    int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 44;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_produk);
        setTitle("Tambah Produk Baru");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        id_usaha = getIntent().getIntExtra("id", id_usaha);
        nama_usaha = getIntent().getStringExtra("nama");

        modelsFoto = new ArrayList<>();
        recyclerView = findViewById(R.id.rv_foto);
        fotoAdapter = new FotoAdapter(this, modelsFoto);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(fotoAdapter);

        spesifikasiModels = new ArrayList<>();
        spesifikasiAdapter = new SpesifikasiAdapter(this, spesifikasiModels);
//        rv_spek = findViewById(R.id.rv_spesifikasi);
//        rv_spek.setLayoutManager(new LinearLayoutManager(this));
//        rv_spek.setHasFixedSize(true);
//        rv_spek.setAdapter(spesifikasiAdapter);

        linearLayout = findViewById(R.id.linearLayout);

        nama =  findViewById(R.id.et_nama);
        harga =  findViewById(R.id.et_harga);
        deskripsi =  findViewById(R.id.et_deskripsi);

        submit =  findViewById(R.id.btn_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelect(v);
//                simpanData();
            }
        });

        btn_foto =  findViewById(R.id.btn_foto);
        btn_foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(TambahProdukActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(TambahProdukActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                } else {
                    if(fotoAdapter.getItemCount() == 5){
                        Toast.makeText(TambahProdukActivity.this, "Foto maksimal hanya 5", Toast.LENGTH_LONG).show();
                    }else{
                        Intent intent = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(intent, 99);
                    }
                }
            }
        });

        btn_spesifikasi =  findViewById(R.id.btn_spesifikasi);
//        btn_spesifikasi.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                tambahSpek();
////                if(spesifikasiAdapter.getItemCount() == 2){
////                    Toast.makeText(TambahProdukActivity.this, "Untuk sementara hanya bisa 2", Toast.LENGTH_LONG).show();
////                }else{
////                    tambahSpek();
////                }
//                tambahSpek();
//            }
//        });

//        spesifikasiAdapter.setListenerSpesifikasi(new SpesifikasiAdapter.ListenerSpesifikasi() {
//            @Override
//            public void callback(View view, int position) {
//                spesifikasiAdapter.removeItem(position);
//            }
//        });

    }

    public void onAddField(View v) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.spesifikasi_item, null);
        // Add the new row before the add field button.
        linearLayout.addView(rowView, linearLayout.getChildCount() - 1);
    }

    public void onDelete(View v) {
        linearLayout.removeView((View) v.getParent());
    }

    public void onSelect(View v) {
        for(int i=0; i < linearLayout.getChildCount(); i++){
            fitur = linearLayout.getChildAt(i).findViewById(R.id.fitur);
            keterangan = linearLayout.getChildAt(i).findViewById(R.id.keterangan);

            SpesifikasiModel spesifikasiModel = new SpesifikasiModel();
            spesifikasiModel.setProperti(fitur.getText().toString());
            spesifikasiModel.setNilai(keterangan.getText().toString());
            spesifikasiModels.add(spesifikasiModel);
        }

        simpanData();
    }

//    private void tambahSpek(){
//        SpesifikasiModel spesifikasiModel = new SpesifikasiModel();
//        spesifikasiModel.setFitur("");
//        spesifikasiModel.setKeterangan("");
//        spesifikasiAdapter.addItem(spesifikasiModel);
//    }

    public List<PostProdukModel.FotoProduk> getFotoProduk(){
        List<PostProdukModel.FotoProduk> fotoProduks = new ArrayList<>();

        for(String path : fotoAdapter.getModels()){
            PostProdukModel.FotoProduk fotoProduk = new PostProdukModel().new FotoProduk();
            fotoProduk.setFoto(convertToBase64(path));
            fotoProduks.add(fotoProduk);
        }

        return fotoProduks;
    }

    public String convertToBase64(String path) {
        Bitmap bm = BitmapFactory.decodeFile(path);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.NO_WRAP);
    }

//    public List<PostProdukModel.Spesifikasi> getSpesifikasi(){
//        List<PostProdukModel.Spesifikasi> spesifikasis = new ArrayList<>();
//        Log.e("data", new Gson().toJson(spesifikasiAdapter.getModels()));
//
//        for(SpesifikasiModel spesifikasiModel : spesifikasiAdapter.getModels()){
//            PostProdukModel.Spesifikasi spesifikasi = new PostProdukModel().new Spesifikasi();
//            spesifikasi.setNilai(spesifikasiModel.getKeterangan());
//            spesifikasi.setProperti(spesifikasiModel.getFitur());
//            spesifikasis.add(spesifikasi);
//        }
//        return spesifikasis;
//    }

    public List<PostProdukModel.Spesifikasi> getSpesifikasi(){
        List<PostProdukModel.Spesifikasi> spesifikasis = new ArrayList<>();

        for(SpesifikasiModel spesifikasiModel : spesifikasiModels){
            PostProdukModel.Spesifikasi spesifikasi = new PostProdukModel().new Spesifikasi();
            spesifikasi.setNilai(spesifikasiModel.getNilai());
            spesifikasi.setProperti(spesifikasiModel.getProperti());
            spesifikasis.add(spesifikasi);
        }

        Log.e("data", new Gson().toJson(spesifikasis));

        return spesifikasis;
    }

    private void simpanData(){
        if(fotoAdapter.getItemCount() == 0){
            Toast.makeText(this, "Foto minimal satu dan maksimal lima!", Toast.LENGTH_SHORT).show();
        }else if(TextUtils.isEmpty(nama.getText().toString())){
            Toast.makeText(this, "Nama tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            nama.requestFocus();
        }else if(TextUtils.isEmpty(harga.getText().toString())){
            Toast.makeText(this, "Harga tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            harga.requestFocus();
        }else if(TextUtils.isEmpty(deskripsi.getText().toString())){
            Toast.makeText(this, "Deskripsi tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            deskripsi.requestFocus();
        }else if(linearLayout.getChildCount() == 0){
            Toast.makeText(this, "Spesifikasi tidak boleh kosong!", Toast.LENGTH_SHORT).show();
        }else{
            final ProgressDialog progressDialog = ProgressDialog.show(this, "Sedang Menyimpan Produk", "Tunggu sebentar...", false, false);

            final PostProdukModel postProdukModel = new PostProdukModel();
            PostProdukModel.Produk produk = postProdukModel.new Produk();
            produk.setNama(nama.getText().toString());
            produk.setHarga(harga.getText().toString());
            produk.setDeskripsi(deskripsi.getText().toString());
            produk.setId_usaha(id_usaha);

            postProdukModel.setProduk(produk);

            postProdukModel.setFoto_produk(getFotoProduk());

            postProdukModel.setSpesifikasi(getSpesifikasi());

            StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_TAMBAH_PRODUK,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                //converting response to json object
                                JSONObject obj = new JSONObject(response);

                                if(obj.getBoolean("success")){
                                    finish();
                                    Toast.makeText(getApplicationContext(), "Produk berhasil ditambahkan", Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(getApplicationContext(), "Produk gagal ditambahkan", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                e.getMessage();
                            }

                            progressDialog.dismiss();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }) {

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {

                    byte[] body = new byte[0];
                    try {
                        String mContent = new Gson().toJson(postProdukModel);
                        Log.e("error", mContent);
                        body = mContent.getBytes("UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        Log.e("error", "Unable to gets bytes from JSON", e.fillInStackTrace());
                    }
                    return body;
                }
            };

            VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            if (requestCode == 99 && resultCode == RESULT_OK
                    && null != data) {


                Uri URI = data.getData();
                String[] FILE = { MediaStore.Images.Media.DATA };


                Cursor cursor = getContentResolver().query(URI,
                        FILE, null, null, null);

                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(FILE[0]);
                String ImageDecode = cursor.getString(columnIndex);
                cursor.close();

                Log.e("Error image", ImageDecode);
                fotoAdapter.setModelItem(ImageDecode);


            }
        } catch (Exception e) {
            Toast.makeText(this, "Please try again", Toast.LENGTH_LONG)
                    .show();
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}

