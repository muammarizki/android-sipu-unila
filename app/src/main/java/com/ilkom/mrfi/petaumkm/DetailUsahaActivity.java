package com.ilkom.mrfi.petaumkm;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.adapters.FeedbackAdapter;
import com.ilkom.mrfi.petaumkm.adapters.ProdukBaruAdapter;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.helpers.VolleySingleton;
import com.ilkom.mrfi.petaumkm.models.FeedbackModel;
import com.ilkom.mrfi.petaumkm.models.ProdukBaruModel;
import com.ilkom.mrfi.petaumkm.models.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by muamm on 1/31/2018.
 */

public class DetailUsahaActivity extends AppCompatActivity {

    private RecyclerView recyclerView, rv_produk;
    List<FeedbackModel> feedbackModels;
    List<ProdukBaruModel> produkModels;
    ProdukBaruAdapter produkBaruAdapter;

    SwipeRefreshLayout swipeRefreshLayout;
    int id_usaha;
    Button btn_lokasi, btn_submit, btn_lengkap;
    ImageView btn_call;
    TextView nama, pemilik, kelas_usaha, alamat, telepon, izin_usaha, tahun, peg_laki, peg_perempuan, jenis_usaha, deskripsi, feedback, selengkapnya;
    RatingBar rating;
    EditText et_feedback;
    String googleMap = "com.google.android.apps.maps";
    Uri gmmIntentUri;
    Intent mapIntent;
    String lokasi, usaha;

    TextView emptyProduk, emptyFeedback;

    FeedbackAdapter feedbackAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_usaha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        id_usaha = getIntent().getIntExtra("id", id_usaha);

        pemilik = findViewById(R.id.pemilik);
        kelas_usaha = findViewById(R.id.kelas_usaha);
        alamat = findViewById(R.id.alamat);
        telepon = findViewById(R.id.telepon);
        izin_usaha = findViewById(R.id.izin_usaha);
        tahun = findViewById(R.id.tahun_data);
        peg_laki = findViewById(R.id.peg_laki);
        peg_perempuan = findViewById(R.id.peg_perempuan);
        jenis_usaha = findViewById(R.id.jenis_usaha);
        deskripsi = findViewById(R.id.deskripsi);
        nama = findViewById(R.id.tv_nama);
        emptyFeedback = findViewById(R.id.emptyViewFeedback);
        emptyProduk = findViewById(R.id.emptyViewProduk);
        feedback = findViewById(R.id.feedback);

        selengkapnya = findViewById(R.id.tv_detail_produk);
        selengkapnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(DetailUsahaActivity.this, IndexProdukUsahaActivity.class);
                i.putExtra("id_usaha", id_usaha);
                i.putExtra("usaha", usaha);
                startActivity(i);
            }
        });

        btn_call = findViewById(R.id.btn_call);

        btn_lokasi = findViewById(R.id.btn_lokasi);
        btn_lokasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gmmIntentUri = Uri.parse("google.navigation:q=" + lokasi);
                mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
                    mapIntent.setPackage(googleMap);
                }

                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                } else {
                    Toast.makeText(DetailUsahaActivity.this, "Install Google Maps dahulu", Toast.LENGTH_LONG).show();
                }
            }
        });

        rv_produk = findViewById(R.id.rv_produk);
        rv_produk.setLayoutManager(new LinearLayoutManager(DetailUsahaActivity.this, LinearLayoutManager.HORIZONTAL, false));
        produkBaruAdapter = new ProdukBaruAdapter(this);
        produkModels = new ArrayList<>();
        getProduk();

        recyclerView = findViewById(R.id.rv_feedback);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        feedbackModels = new ArrayList<>();
        feedbackAdapter = new FeedbackAdapter(DetailUsahaActivity.this);

        getFeedback(id_usaha);

        btn_submit = findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogAddItem();
            }
        });

        btn_lengkap = findViewById(R.id.btn_lengkap);
        btn_lengkap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DetailUsahaActivity.this, RatingFeedbackActivity.class);
                i.putExtra("id", id_usaha);
                startActivity(i);
            }
        });

        getUsaha(id_usaha);
    }

    private void getProduk() {
        if (produkModels.size() > 0) {
            produkModels.clear();
            produkBaruAdapter.clearModel();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_GET_PRODUK_USAHA_LIMIT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject o = array.getJSONObject(i);
                                produkModels.add(new ProdukBaruModel(
                                        o.getInt("id"),
                                        o.getString("nama"),
                                        o.getString("harga"),
                                        o.getString("thumbnail"),
                                        o.getInt("pengunjung"),
                                        o.getString("usaha"),
                                        o.getString("kota")
                                ));
                            }

                            produkBaruAdapter.setProdukModels(produkModels);
                            rv_produk.setAdapter(produkBaruAdapter);

                            if(produkBaruAdapter.getItemCount() == 0){
                                rv_produk.setVisibility(View.GONE);
                                emptyProduk.setVisibility(View.VISIBLE);
                                selengkapnya.setVisibility(View.GONE);
                            }else{
                                rv_produk.setVisibility(View.VISIBLE);
                                selengkapnya.setVisibility(View.VISIBLE);
                                emptyProduk.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_usaha", String.valueOf(id_usaha));
                return params;
            }
        };
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(getApplicationContext()).add(stringRequest);
    }

    private void getFeedback(int id) {
        if (feedbackModels.size() > 0) {
            feedbackModels.clear();
            feedbackAdapter.clearModel();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_GET_FEEDBACK_USAHA_LIMIT + id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject o = array.getJSONObject(i);
                                feedbackModels.add(new FeedbackModel(
                                        o.getString("id"),
                                        o.getString("nama"),
                                        o.getInt("rating"),
                                        o.getString("feedback"),
                                        o.getString("waktu"),
                                        o.getString("foto_profil")
                                ));
                            }

                            feedbackAdapter.setFeedbackModels(feedbackModels);
                            recyclerView.setAdapter(feedbackAdapter);

                            if(feedbackAdapter.getItemCount() == 0){
                                recyclerView.setVisibility(View.GONE);
                                emptyFeedback.setVisibility(View.VISIBLE);
                                btn_lengkap.setVisibility(View.GONE);
                            }else{
                                recyclerView.setVisibility(View.VISIBLE);
                                emptyFeedback.setVisibility(View.GONE);
                                btn_lengkap.setVisibility(View.VISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DetailUsahaActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);
    }

    private void DialogAddItem() {
        LayoutInflater inflater;
        AlertDialog.Builder dialog;
        View dialogView;
        dialog = new AlertDialog.Builder(DetailUsahaActivity.this);

        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.dialog_feedback, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setTitle("Tambah Feedback");

        rating = (RatingBar) dialogView.findViewById(R.id.rating);
        et_feedback = (EditText) dialogView.findViewById(R.id.et_feedback);

        dialog.setPositiveButton("Tambah", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                submitFeedback();
                dialog.dismiss();
            }
        });
        dialog.setNegativeButton("Batalkan", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void submitFeedback() {
        UserModel user = SharedPrefManager.getInstance(this).getUser();

        final String ratings = String.valueOf(Math.round(rating.getRating()));
        final String feedback = et_feedback.getText().toString().trim();
        final String id_pemberi = String.valueOf(user.getId());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_FEEDBACK_USAHA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);

                            //if no error in response
                            if (obj.getBoolean("success")) {

                                Toast.makeText(getApplicationContext(), "Terimakasih atas feedbacknya", Toast.LENGTH_SHORT).show();
//                                startActivity(new Intent(getApplicationContext(), DetailUsahaActivity.class).putExtra("id", id_usaha));
//                                finish();
                                getFeedback(id_usaha);

                            } else {
                                Toast.makeText(getApplicationContext(), "Gagal", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("rating", ratings);
                params.put("feedback", feedback);
                params.put("id_pemberi", id_pemberi);
                params.put("id_usaha", String.valueOf(id_usaha));
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void getUsaha(final int id_usaha) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_DETAIL_USAHA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                            if (obj.getBoolean("success")) {
                                final JSONObject detail = obj.getJSONObject("message");

                                nama.setText(detail.getString("nama"));
                                pemilik.setText(detail.getString("pelaku"));
                                kelas_usaha.setText((detail.getString("aset")));
                                alamat.setText(detail.getString("alamat"));
                                telepon.setText(detail.getString("no_telp"));
                                izin_usaha.setText(detail.getString("no_izin_usaha") + " " + detail.getString("izin_usaha"));
                                tahun.setText(detail.getString("tahun_data"));
                                peg_laki.setText("Pria (" + detail.getString("peg_lk") + " orang)");
                                peg_perempuan.setText("Wanita (" + detail.getString("peg_pr") + " orang)");
                                jenis_usaha.setText(detail.getString("jenis_usaha"));
                                deskripsi.setText(detail.getString("deskripsi"));
                                feedback.setText(detail.getString("rating") + " " + "(" + detail.getString("feedback") + " feedback)");

                                lokasi = detail.getString("latitude") + ", " + detail.getString("longitude");

                                setTitle(detail.getString("nama"));

                                usaha = detail.getString("nama");

                                btn_call.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(Intent.ACTION_DIAL);
                                        try {
                                            intent.setData(Uri.parse("tel:" + detail.getString("no_telp")));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        startActivity(intent);
                                    }
                                });

                            }else{
                                Toast.makeText(DetailUsahaActivity.this, "Agaknya data lo ilang bro",Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DetailUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_usaha", String.valueOf(id_usaha));
                return params;
            }
        };
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
