package com.ilkom.mrfi.petaumkm;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.adapters.ProdukBaruAdapter;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.ProdukBaruModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static java.security.AccessController.getContext;
import static java.util.Locale.filter;

/**
 * Created by muamm on 3/6/2018.
 */

public class ProdukBaruActivity extends AppCompatActivity{

    RecyclerView rv_produk;
    List<ProdukBaruModel> produkBaruModels;
    ProdukBaruAdapter produkBaruAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produk_baru);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        int columns = 2;
        rv_produk = findViewById(R.id.rv_list_produk);
        rv_produk.setLayoutManager(new GridLayoutManager(this, columns));
        produkBaruModels = new ArrayList<>();
        produkBaruAdapter = new ProdukBaruAdapter(ProdukBaruActivity.this);

        getProduk();
    }

    private void getProduk(){
        if(produkBaruModels.size()>0){
            produkBaruModels.clear();
            produkBaruAdapter.clearModel();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_GET_PRODUK_BARU_ALL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length();i++){
                                JSONObject o = array.getJSONObject(i);
                                produkBaruModels.add(new ProdukBaruModel(
                                        o.getInt("id"),
                                        o.getString("nama"),
                                        o.getString("harga"),
                                        o.getString("thumbnail"),
                                        o.getInt("pengunjung"),
                                        o.getString("usaha"),
                                        o.getString("kota")
                                ));
                            }

                            produkBaruAdapter.setProdukModels(produkBaruModels);
                            rv_produk.setAdapter(produkBaruAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ProdukBaruActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(ProdukBaruActivity.this).add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Cari Produk Terbaru...");
        searchView.setIconifiedByDefault(false);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                final List<ProdukBaruModel> filteredModelList = filter(produkBaruModels, s);

                produkBaruAdapter.setFilter(filteredModelList);
                return false;
            }
        });

        return true;
    }

    private List<ProdukBaruModel> filter(List<ProdukBaruModel> models, String query) {
        query = query.toLowerCase();final List<ProdukBaruModel> filteredModelList = new ArrayList<>();
        for (ProdukBaruModel model : models) {
            final String text = model.getNama().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        produkBaruModels.clear();
        getProduk();
    }
}
