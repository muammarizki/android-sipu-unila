package com.ilkom.mrfi.petaumkm.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by muamm on 1/27/2018.
 */

public class ProvinsiModel {

    @SerializedName("id")
    private int provinsiId;

    @SerializedName("nama")
    private String namaProvinsi;

    public int getProvinsiId() {
        return provinsiId;
    }

    public void setProvinsiId(int provinsiId) {
        this.provinsiId = provinsiId;
    }

    public String getNamaProvinsi() {
        return namaProvinsi;
    }

    public void setNamaProvinsi(String namaProvinsi) {
        this.namaProvinsi = namaProvinsi;
    }

    @Override
    public String toString(){
        return namaProvinsi;
    }
}
