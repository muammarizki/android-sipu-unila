package com.ilkom.mrfi.petaumkm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.UserModel;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by muamm on 12/19/2017.
 */

public class FragmentProfile extends Fragment {

    TextView tv_nama,tv_tipe, tv_pengaturan,tv_bantuan,tv_kelola_usaha,tv_kelola_produk,tv_biodata;
    CircleImageView foto_profil;

    private View view;
    CoordinatorLayout coordinatorLayout;
    SwipeRefreshLayout swipeRefreshLayout;
    Snackbar snackbar;

    public FragmentProfile() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_fragment_profile, container, false);

        UserModel user = SharedPrefManager.getInstance(getActivity()).getUser();

        if (!SharedPrefManager.getInstance(getActivity()).isLoggedIn()) {
            getActivity().finish();
            startActivity(new Intent(getActivity(), LoginActivity.class));
        }

        coordinatorLayout = view.findViewById(R.id.coordinator);

        tv_nama = view.findViewById(R.id.tv_nama);

        foto_profil = view.findViewById(R.id.profile);

        tv_tipe = view.findViewById(R.id.tv_peran);

        tv_biodata = view.findViewById(R.id.tv_biodata);

        tv_pengaturan = view.findViewById(R.id.tv_pengaturan);
        tv_pengaturan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pengaturan = new Intent(getActivity(), PengaturanActivity.class);
                startActivity(pengaturan);
            }
        });

        tv_bantuan = view.findViewById(R.id.tv_bantuan);
        tv_bantuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bantuan = new Intent(getActivity(), BantuanActivity.class);
                startActivity(bantuan);
            }
        });

        tv_kelola_usaha = view.findViewById(R.id.tv_kelola_usaha);
        tv_kelola_usaha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent usaha = new Intent(getActivity(), KelolaUsahaActivity.class);
                startActivity(usaha);
            }
        });

        tv_kelola_produk = view.findViewById(R.id.tv_kelola_produk);
        tv_kelola_produk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent produk = new Intent(getActivity(), KelolaProdukActivity.class);
                startActivity(produk);
            }
        });

        if(user.getPeran() == 4){
            tv_kelola_produk.setVisibility(View.GONE);
            tv_kelola_usaha.setVisibility(View.GONE);
            tv_biodata.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(getActivity(), DetailBiodataPelangganActivity.class);
                    startActivity(i);
                }
            });
        }else{
            tv_kelola_produk.setVisibility(View.VISIBLE);
            tv_kelola_usaha.setVisibility(View.VISIBLE);
            tv_biodata.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(getActivity(), DetailBiodataActivity.class);
                    startActivity(i);
                }
            });
        }

        snackbar = Snackbar
                .make(coordinatorLayout, "Tidak Ada Koneksi", Snackbar.LENGTH_LONG)
                .setAction("TUTUP", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        snackbar.dismiss();
                    }
                });

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!isNetworkConnected()){
                    snackbar.show();
                    swipeRefreshLayout.setRefreshing(false);
                }else{
                    getUser();
                    snackbar.dismiss();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        if(!isNetworkConnected()){
            snackbar.show();
        }else{
            snackbar.dismiss();
        }

        getUser();

        return view;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void getUser(){
        final UserModel user = SharedPrefManager.getInstance(getActivity()).getUser();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_DETAIL_USER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                            JSONObject o = obj.getJSONObject("message");

                            if(obj.getBoolean("success")){

                              Picasso.with(getContext())
                                        .load("http://sipu.capung.tech/api/user/foto-profil/get/" + o.getString("foto_profil"))
                                        .placeholder(R.drawable.avatar)
                                        .into(foto_profil);

                                tv_nama.setText(o.getString("nama"));

                                if(o.getInt("id_peran") == 2){
                                    tv_tipe.setText("Pelaku UMKM");
                                }else{
                                    tv_tipe.setText("Pelanggan");
                                }
                            }else{
                                Toast.makeText(getActivity(), "User tidak ditemukan!", Toast.LENGTH_LONG).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(getActivity(), error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", String.valueOf(user.getId()));
                return params;
            }
        };
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(getActivity()).add(stringRequest);
    }


    @Override
    public void onStart() {
        super.onStart();
        getUser();
    }

    public void onResume(){
        super.onResume();
        ((MainActivity) getActivity()).setActionBarTitle("Profil");
    }

}
