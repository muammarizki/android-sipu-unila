package com.ilkom.mrfi.petaumkm;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.UserModel;
import com.ilkom.mrfi.petaumkm.helpers.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by muamm on 12/20/2017.
 */

public class RegisterActivity extends AppCompatActivity {
    private Button btn_register;
    private TextView tv_login;
    private EditText et_password2,et_nama,et_username,et_email,et_password;
    private RadioGroup rg_peran;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        if (SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, MainActivity.class));
            return;
        }

        et_nama = findViewById(R.id.et_nama);
        et_username = findViewById(R.id.et_username);
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        et_password2 = findViewById(R.id.et_password2);
        rg_peran = findViewById(R.id.rg_peran);

        btn_register = findViewById(R.id.btn_daftar);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userRegister();
            }
        });

        tv_login = findViewById(R.id.tv_login);
        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

    }

    private void userRegister(){
        final String nama = et_nama.getText().toString().trim();
        final String username = et_username.getText().toString().trim();
        final String email = et_email.getText().toString().trim();
        final String password = et_password.getText().toString().trim();
        final String password2 = et_password2.getText().toString().trim();

        final String peran = ((RadioButton) findViewById(rg_peran.getCheckedRadioButtonId())).getText().toString();

        if(TextUtils.isEmpty(nama)){
            et_nama.setError("Nama tidak boleh kosong!"); // show error
            et_nama.requestFocus(); // hide error
            return;
        }

        if(TextUtils.isEmpty(username)){
            et_username.setError("Username tidak boleh kosong!"); // show error
            et_username.requestFocus(); // hide error
            return;
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            et_email.setError("Email tidak valid!");
            et_email.requestFocus();
            return;
        }

        if (password.length() < 6) {
            et_password.setError("Password minimum 6 karakter");
            et_password.requestFocus();
            return;
        }

        if (!isMatch(password, password2)) {
            et_password2.setError("Password tidak sama!");
            et_password2.requestFocus();
            return;
        }

        final ProgressDialog progressDialog = ProgressDialog.show(this, "", "Tunggu sebentar...", false, false);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_REGISTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);

                            //if no error in response
                            if (obj.getBoolean("success")) {

                                //getting the user from the response
                                JSONObject userJson = obj.getJSONObject("message");

                                //creating a new user object
                                UserModel user = new UserModel(
                                        userJson.getInt("id"),
                                        userJson.getInt("id_peran"),
                                        userJson.getString("username")
                                );

                                //storing the user in shared preferences
                                SharedPrefManager.getInstance(getApplicationContext()).userLogin(user);

                                if(userJson.getInt("id_peran") == 2){
                                    finish();
                                    startActivity(new Intent(getApplicationContext(), TambahBiodataActivity.class));
                                }else{
                                    finish();
//                                    Toast.makeText(getApplicationContext(), "Selamat Datang " + userJson.getString("nama"), Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("nama", nama);
                params.put("username", username);
                params.put("email", email);
                params.put("password", password2);
                if(peran.equals("Pelanggan")) {
                    params.put("id_peran", "4");
                }else{
                    params.put("id_peran", "2");
                }
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    private boolean isMatch(String password, String password2){
        return password.equals(password2);
    }
}
