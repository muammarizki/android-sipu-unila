package com.ilkom.mrfi.petaumkm.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.ilkom.mrfi.petaumkm.R;
import com.ilkom.mrfi.petaumkm.models.InfoWindowData;

/**
 * Created by muamm on 3/20/2018.
 */

public class CustomWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private Context context;
    private TextView nama, telepon, alamat, deskripsi;

    public CustomWindowAdapter(Context ctx){
        context = ctx;
    }

    @Override
    public View getInfoWindow(Marker marker){
        return null;
    }

    public View getInfoContents(Marker marker){
        View view = ((Activity)context).getLayoutInflater().inflate(R.layout.custom_window, null);

        nama = view.findViewById(R.id.nama);
        deskripsi = view.findViewById(R.id.deskripsi);
        alamat = view.findViewById(R.id.alamat);
        telepon = view.findViewById(R.id.telepon);

        nama.setText(marker.getTitle());
        deskripsi.setText(marker.getSnippet());

        InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();

        alamat.setText(infoWindowData.getAlamat());
        telepon.setText(infoWindowData.getTelepon());

        return view;
    }
}













