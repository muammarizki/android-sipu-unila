package com.ilkom.mrfi.petaumkm.models;

/**
 * Created by muamm on 2/21/2018.
 */

public class ProdukGridModel {
    int id;
    String nama;
    String harga;
    int pengunjung;
    String thumbnail;
    String umkm;

    public ProdukGridModel(int id, String nama, String harga, int pengunjung, String thumbnail, String umkm) {
        this.id = id;
        this.nama = nama;
        this.harga = harga;
        this.pengunjung = pengunjung;
        this.thumbnail = thumbnail;
        this.umkm = umkm;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public int getPengunjung() {
        return pengunjung;
    }

    public void setPengunjung(int pengunjung) {
        this.pengunjung = pengunjung;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getUmkm() {
        return umkm;
    }

    public void setUmkm(String umkm) {
        this.umkm = umkm;
    }
}
