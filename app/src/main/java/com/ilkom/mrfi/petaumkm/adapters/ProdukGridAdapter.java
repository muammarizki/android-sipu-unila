package com.ilkom.mrfi.petaumkm.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.DetailProdukActivity;
import com.ilkom.mrfi.petaumkm.EditProdukActivity;
import com.ilkom.mrfi.petaumkm.R;
import com.ilkom.mrfi.petaumkm.helpers.HargaTextView;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.ProdukGridModel;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by muamm on 2/21/2018.
 */

public class ProdukGridAdapter extends RecyclerView.Adapter<ProdukGridAdapter.ProdukViewHolder> {

    private Context context;
    private List<ProdukGridModel> produkGridModelList;
    ProdukGridModel produk;

    public ProdukGridAdapter(Context context){
        this.context = context;
        this.produkGridModelList = new ArrayList<>();
    }

    public ProdukViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.produk_usaha_item, null);
        return new ProdukGridAdapter.ProdukViewHolder(view);
    }

    public void setFilter(List<ProdukGridModel> produkModels) {
        produkGridModelList = new ArrayList<>();
        produkGridModelList.addAll(produkModels);
        notifyDataSetChanged();
    }

    public void setProdukModels(List<ProdukGridModel> feedbackModels) {
        this.produkGridModelList = feedbackModels;
        notifyDataSetChanged();
    }

    public void clearModel(){
        produkGridModelList.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final ProdukGridAdapter.ProdukViewHolder holder, final int position) {
        produk = produkGridModelList.get(position);

        holder.tv_nama.setText(produk.getNama());
        holder.tv_harga.setText(produk.getHarga());
        holder.tv_pengunjung.setText(String.valueOf(produk.getPengunjung()));
        Picasso.with(context)
                .load("http://sipu.capung.tech/api/produk/thumb/get/" + produk.getThumbnail())
                .into(holder.thumbnail);

        holder.options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating a popup menu
                final PopupMenu popup = new PopupMenu(context, holder.options);
                //inflating menu from xml resource
                popup.inflate(R.menu.usaha_options);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.lihat:
                                Intent intent = new Intent(context, DetailProdukActivity.class);
                                intent.putExtra("id_produk", produkGridModelList.get(position).getId());
                                context.startActivity(intent);
                                break;
                            case R.id.edit:
                                Intent i = new Intent(context, EditProdukActivity.class);
                                i.putExtra("id_produk", produkGridModelList.get(position).getId());
                                context.startActivity(i);
                                break;
                            case R.id.hapus:
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Konfirmasi");
                                builder.setMessage("Apakah anda yakin ingin menghapus\n" +
                                        produkGridModelList.get(position).getNama() + " ?");
                                builder.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        hapusProduk(produkGridModelList.get(position).getId());
                                        produkGridModelList.remove(position);
                                        notifyDataSetChanged();
                                        dialog.dismiss();
                                    }
                                });
                                builder.setNegativeButton("Batalkan", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                AlertDialog alert = builder.create();
                                alert.show();
                                break;
                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();
            }
        });
    }

    private void hapusProduk(final int id){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_HAPUS_PRODUK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                            if(obj.getBoolean("success")){
                                Toast.makeText(context, obj.getString("message"),Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(context, obj.getString("message"),Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_produk", String.valueOf(id));
                return params;
            }
        };

        Volley.newRequestQueue(context).add(stringRequest);
    }

    @Override
    public int getItemCount() {
        return produkGridModelList.size();
    }

    class ProdukViewHolder extends RecyclerView.ViewHolder {

        ImageView thumbnail, options;
        TextView tv_nama, tv_pengunjung;
        HargaTextView tv_harga;

        public ProdukViewHolder(View itemView) {
            super(itemView);

            thumbnail = itemView.findViewById(R.id.thumbnail);
            tv_nama = itemView.findViewById(R.id.tv_nama);
            tv_harga = itemView.findViewById(R.id.tv_harga);
            tv_pengunjung = itemView.findViewById(R.id.pengunjung);
            options = itemView.findViewById(R.id.options);
        }
    }
    
}
