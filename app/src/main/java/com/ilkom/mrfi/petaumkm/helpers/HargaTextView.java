package com.ilkom.mrfi.petaumkm.helpers;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Created by muamm on 3/13/2018.
 */

public class HargaTextView extends AppCompatTextView {

    String rawText;

    public HargaTextView(Context context){
        super(context);
    }

    public HargaTextView(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    public  HargaTextView(Context context, AttributeSet attrs, int defStyleAttr){
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setText(CharSequence text, BufferType type){
        rawText = text.toString();
        String prezzo = text.toString();
        try {
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator('.');
            DecimalFormat decimalFormat = new DecimalFormat("Rp ###,###,###,###", symbols);
            prezzo = decimalFormat.format(Integer.parseInt(text.toString()));
        }catch (Exception e){

        }

        super.setText(prezzo, type);
    }

    @Override
    public CharSequence getText(){
        return rawText;
    }


}
