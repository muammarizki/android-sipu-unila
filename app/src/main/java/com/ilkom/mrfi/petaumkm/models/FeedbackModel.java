package com.ilkom.mrfi.petaumkm.models;

/**
 * Created by muamm on 2/6/2018.
 */

public class FeedbackModel {

    private String id;
    private String nama;
    private String feedback;
    private float rating;
    private String waktu;
    private String foto;

    public FeedbackModel(String id, String nama, float rating, String feedback, String waktu, String foto){
        this.id = id;
        this.nama = nama;
        this.rating = rating;
        this.feedback = feedback;
        this.waktu = waktu;
        this.foto = foto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
