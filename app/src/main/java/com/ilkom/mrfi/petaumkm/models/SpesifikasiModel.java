package com.ilkom.mrfi.petaumkm.models;


/**
 * Created by muamm on 2/7/2018.
 */

public class SpesifikasiModel {

    String properti, nilai;

    public SpesifikasiModel() {

    }

//    public SpesifikasiModel(String fitur, String keterangan){
//        this.fitur = fitur;
//        this.keterangan = keterangan;
//    }


    public String getProperti() {
        return properti;
    }

    public void setProperti(String properti) {
        this.properti = properti;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }
}
