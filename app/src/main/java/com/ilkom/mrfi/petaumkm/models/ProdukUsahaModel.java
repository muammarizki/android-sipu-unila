package com.ilkom.mrfi.petaumkm.models;

/**
 * Created by muamm on 2/1/2018.
 */

public class ProdukUsahaModel {
    int id;
    String nama, deskripsi;

    public ProdukUsahaModel(int id, String nama, String deskripsi){
        this.id = id;
        this.nama = nama;
        this.deskripsi = deskripsi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
