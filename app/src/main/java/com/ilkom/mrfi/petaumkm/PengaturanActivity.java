package com.ilkom.mrfi.petaumkm;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.models.UserModel;

import org.w3c.dom.Text;

/**
 * Created by muamm on 1/2/2018.
 */

public class PengaturanActivity extends AppCompatActivity {
    TextView tv_logout,tv_edit_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaturan);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        UserModel user = SharedPrefManager.getInstance(this).getUser();

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        tv_edit_password = findViewById(R.id.tv_edit_password);
        tv_edit_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PengaturanActivity.this, EditPasswordActivity.class);
                startActivity(i);
            }
        });

        tv_logout = findViewById(R.id.tv_logout);
        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                SharedPrefManager.getInstance(getApplicationContext()).logout();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
