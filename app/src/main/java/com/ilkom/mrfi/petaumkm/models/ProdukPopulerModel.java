package com.ilkom.mrfi.petaumkm.models;

/**
 * Created by muamm on 3/1/2018.
 */

public class ProdukPopulerModel {

    int id;
    String nama;
    String harga;
    int pengunjung;
    String thumbnail;
    String umkm;
    String kota;

    public ProdukPopulerModel(int id, String nama, String harga, String thumbnail, int pengunjung, String umkm, String kota) {
        this.id = id;
        this.nama = nama;
        this.harga = harga;
        this.thumbnail = thumbnail;
        this.pengunjung = pengunjung;
        this.umkm = umkm;
        this.kota = kota;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public int getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getHarga() {
        return harga;
    }

    public int getPengunjung() {
        return pengunjung;
    }

    public String getUmkm() {
        return umkm;
    }

    public String getKota() {
        return kota;
    }
}
