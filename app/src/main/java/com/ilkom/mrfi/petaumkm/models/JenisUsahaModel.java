package com.ilkom.mrfi.petaumkm.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by muamm on 1/27/2018.
 */

public class JenisUsahaModel {

    @SerializedName("id")
    private int jenisUsahaId;

    @SerializedName("id_sektor_usaha")
    private int sektorUsahaId;

    @SerializedName("nama")
    private String namaJenisUsaha;

    public int getJenisUsahaId() {
        return jenisUsahaId;
    }

    public void setJenisUsahaId(int jenisUsahaId) {
        this.jenisUsahaId = jenisUsahaId;
    }

    public int getSektorUsahaId() {
        return sektorUsahaId;
    }

    public void setSektorUsahaId(int sektorUsahaId) {
        this.sektorUsahaId = sektorUsahaId;
    }

    public String getNamaJenisUsaha() {
        return namaJenisUsaha;
    }

    public void setNamaJenisUsaha(String namaJenisUsaha) {
        this.namaJenisUsaha = namaJenisUsaha;
    }

    @Override
    public String toString(){
        return namaJenisUsaha;
    }
}
