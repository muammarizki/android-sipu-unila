package com.ilkom.mrfi.petaumkm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.ilkom.mrfi.petaumkm.R;
import com.ilkom.mrfi.petaumkm.models.SpesifikasiModel;

import java.util.List;

/**
 * Created by muamm on 2/7/2018.
 */

public class SpesifikasiAdapter extends RecyclerView.Adapter<SpesifikasiAdapter.SpesifikasiViewHolder> {

    private ListenerSpesifikasi listenerSpesifikasi;
    private Context context;
    private List<SpesifikasiModel> models;

    public SpesifikasiAdapter(Context context, List<SpesifikasiModel> models){
        this.context = context;
        this.models = models;
    }

    public List<SpesifikasiModel> getModels(){
        return models;
    }

    public void addItem(SpesifikasiModel spesifikasiModel){
        models.add(spesifikasiModel);
        notifyItemInserted(models.size() - 1);
    }

    public void removeItem(int position){
        models.get(position).setFitur("");
        models.get(position).setKeterangan("");
        models.remove(position);
        notifyItemRangeRemoved(0, models.size());
    }

    public void setListenerSpesifikasi(ListenerSpesifikasi listenerSpesifikasi) {
        this.listenerSpesifikasi = listenerSpesifikasi;
    }

    public SpesifikasiViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.spesifikasi_item, parent, false);
        return new SpesifikasiViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SpesifikasiViewHolder holder, final int position) {
        final SpesifikasiModel spek = models.get(position);

    }

    @Override
    public int getItemCount() {
        return models != null ? models.size() : 0;
    }

    class SpesifikasiViewHolder extends RecyclerView.ViewHolder {

        EditText fitur, keterangan;
        ImageView hapus;

        public SpesifikasiViewHolder(final View itemView) {
            super(itemView);

            fitur = itemView.findViewById(R.id.fitur);
            keterangan = itemView.findViewById(R.id.keterangan);
            hapus = itemView.findViewById(R.id.hapus);

            hapus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listenerSpesifikasi != null){
                        listenerSpesifikasi.callback(v, getAdapterPosition());
                    }
                }
            });

            fitur.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() > 0) {
                        models.get(getAdapterPosition()).setProperti(fitur.getText().toString());
                    }
                }
            });

            keterangan.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.length() > 0) {
                        models.get(getAdapterPosition()).setNilai(keterangan.getText().toString());
                    }
                }
            });

        }

    }

    public interface ListenerSpesifikasi{
        void callback(View view, int position);
    }
}
