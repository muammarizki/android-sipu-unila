package com.ilkom.mrfi.petaumkm;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.ilkom.mrfi.petaumkm.helpers.MapsFragment;
import com.ilkom.mrfi.petaumkm.helpers.PermissionUtils;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.helpers.VolleySingleton;
import com.ilkom.mrfi.petaumkm.models.AsetModel;
import com.ilkom.mrfi.petaumkm.models.DesaModel;
import com.ilkom.mrfi.petaumkm.models.IzinUsahaModel;
import com.ilkom.mrfi.petaumkm.models.JenisUsahaModel;
import com.ilkom.mrfi.petaumkm.models.KabupatenModel;
import com.ilkom.mrfi.petaumkm.models.KecamatanModel;
import com.ilkom.mrfi.petaumkm.models.MarkerModel;
import com.ilkom.mrfi.petaumkm.models.OmsetModel;
import com.ilkom.mrfi.petaumkm.models.ProvinsiModel;
import com.ilkom.mrfi.petaumkm.models.SektorUsahaModel;
import com.ilkom.mrfi.petaumkm.models.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by muamm on 2/1/2018.
 */

public class EditUsahaActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMyLocationClickListener,
        GoogleMap.OnMyLocationButtonClickListener, ActivityCompat.OnRequestPermissionsResultCallback {

    ArrayAdapter<ProvinsiModel> adapterProvinsi;
    ArrayAdapter<KabupatenModel> adapterKabupaten;
    ArrayAdapter<KecamatanModel> adapterKecamatan;
    ArrayAdapter<DesaModel> adapterDesa;
    ArrayAdapter<OmsetModel> adapterOmset;
    ArrayAdapter<JenisUsahaModel> adapterJenisUsaha;
    ArrayAdapter<IzinUsahaModel> adapterIzinUsaha;
    ArrayAdapter<SektorUsahaModel> adapterSektorUsaha;
    ArrayAdapter<AsetModel> adapterAset;

    EditText et_nama,et_deskripsi,et_alamat,et_telepon,et_tahun,pegpr,peglk,et_noIzinUsaha,et_latitude,et_longitude;
    Button btn_simpan;
    Spinner sp_provinsi, sp_kabupaten, sp_kecamatan, sp_desa, sp_omset, sp_izin, sp_jenis, sp_sektor, sp_aset;

    private List<ProvinsiModel> provinsiModelList = new ArrayList<>();
    private List<KabupatenModel> kabupatenModelList = new ArrayList<>();
    private List<KecamatanModel> kecamatanModelList = new ArrayList<>();
    private List<DesaModel> desaModelList = new ArrayList<>();
    private List<OmsetModel> omsetModelList = new ArrayList<>();
    private List<JenisUsahaModel> jenisUsahaModelList = new ArrayList<>();
    private List<IzinUsahaModel> izinUsahaModelList = new ArrayList<>();
    private List<AsetModel> asetModelList = new ArrayList<>();
    private List<SektorUsahaModel> sektorUsahaModelList = new ArrayList<>();

    ScrollView mScrollView;
    GoogleMap mMap;
    CameraPosition cameraPosition;
    LatLng center;
    double latitude, longitude, latitudes, longitudes;
    String desa,omset,izinUsaha,jenisUsaha,nama_usaha, sektorUsaha, aset;
    int id_usaha;
    private boolean firstData = true;
    private boolean firstData2 = true;
    private JSONObject detail;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private boolean mPermissionDenied = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_usaha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        id_usaha = getIntent().getIntExtra("id", id_usaha);
        nama_usaha = getIntent().getStringExtra("nama");
        setTitle("Edit " + nama_usaha);

        ((MapsFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map)).getMapAsync(this);
        mScrollView = findViewById(R.id.sv_container);

        ((MapsFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map)).setListener(new MapsFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                mScrollView.requestDisallowInterceptTouchEvent(true);
            }
        });

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        sp_provinsi = findViewById(R.id.sp_provinsi);
        sp_provinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String provinsi = String.valueOf(provinsiModelList.get(position).getProvinsiId());
                getKabupaten(provinsi);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_kabupaten = findViewById(R.id.sp_kabupaten);
        sp_kabupaten.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String kabupaten = String.valueOf(kabupatenModelList.get(position).getKabupatenId());
                getKecamatan(kabupaten);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_kecamatan = findViewById(R.id.sp_kecamatan);
        sp_kecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String kecamatan = String.valueOf(kecamatanModelList.get(position).getKecamatanId());
                getDesa(kecamatan);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_desa = findViewById(R.id.sp_desa);
        sp_desa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                desa = String.valueOf(desaModelList.get(position).getDesaId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_omset = findViewById(R.id.sp_omset);
        getOmset();
        sp_omset.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                omset = String.valueOf(omsetModelList.get(position).getOmsetId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_aset = findViewById(R.id.sp_aset);
        getAset();
        sp_aset.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                aset = String.valueOf(asetModelList.get(i).getAsetId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });

        sp_izin = findViewById(R.id.sp_izin_usaha);
        getIzinUsaha();
        sp_izin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                izinUsaha = String.valueOf(izinUsahaModelList.get(position).getIzinUsahaId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_sektor = findViewById(R.id.sp_sektor_usaha);
        sp_sektor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                sektorUsaha = String.valueOf(sektorUsahaModelList.get(i).getSektorUsahaId());
                getJenisUsaha(sektorUsaha);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sp_jenis = findViewById(R.id.sp_jenis_usaha);
        sp_jenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                jenisUsaha = String.valueOf(jenisUsahaModelList.get(position).getJenisUsahaId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        et_latitude = findViewById(R.id.et_latitude);
        et_latitude.setEnabled(false);

        et_longitude = findViewById(R.id.et_longitude);
        et_longitude.setEnabled(false);

        et_nama = findViewById(R.id.et_nama);
        et_deskripsi = findViewById(R.id.et_deskripsi);
        et_telepon = findViewById(R.id.et_telepon);
        et_alamat = findViewById(R.id.et_alamat);
        et_noIzinUsaha = findViewById(R.id.et_no_izin_usaha);
        peglk = findViewById(R.id.et_peglk);
        pegpr = findViewById(R.id.et_pegpr);
        et_tahun = findViewById(R.id.et_tahun);

        btn_simpan = findViewById(R.id.btn_simpan);
        btn_simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateData();
            }
        });

        adapterSektorUsaha = new ArrayAdapter<>(EditUsahaActivity.this, android.R.layout.simple_spinner_item, sektorUsahaModelList);adapterJenisUsaha = new ArrayAdapter<>(EditUsahaActivity.this, android.R.layout.simple_spinner_item, jenisUsahaModelList);
        adapterJenisUsaha = new ArrayAdapter<>(EditUsahaActivity.this, android.R.layout.simple_spinner_item, jenisUsahaModelList);
        adapterProvinsi = new ArrayAdapter<>(EditUsahaActivity.this, android.R.layout.simple_spinner_item, provinsiModelList);
        adapterKabupaten = new ArrayAdapter<>(EditUsahaActivity.this, android.R.layout.simple_spinner_item, kabupatenModelList);
        adapterKecamatan = new ArrayAdapter<>(EditUsahaActivity.this, android.R.layout.simple_spinner_item, kecamatanModelList);
        adapterDesa = new ArrayAdapter<>(EditUsahaActivity.this, android.R.layout.simple_spinner_item, desaModelList);

        loadData();

    }

    private void updateData(){
        final ProgressDialog progressDialog = ProgressDialog.show(this, "Memperbaharui Data Usaha", "Tunggu sebentar...", false, false);

        UserModel user = SharedPrefManager.getInstance(this).getUser();

        final String nama = et_nama.getText().toString().trim();
        final String deskripsi = et_deskripsi.getText().toString().trim();
        final String alamat = et_alamat.getText().toString().trim();
        final String telepon = et_telepon.getText().toString().trim();
        final String tahun = et_tahun.getText().toString().trim();
        final String pegPria = peglk.getText().toString().trim();
        final String noIzinUsaha = et_noIzinUsaha.getText().toString().trim();
        final String pegWanita = pegpr.getText().toString().trim();
        final String latitude = et_latitude.getText().toString().trim();
        final String longitude = et_longitude.getText().toString().trim();
        final String userId = String.valueOf(user.getId());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_EDIT_USAHA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);

                            //if no error in response
                            if (obj.getBoolean("success")) {

                                Toast.makeText(getApplicationContext(), "Data berhasil diubah", Toast.LENGTH_SHORT).show();
//                                startActivity(new Intent(getApplicationContext(), KelolaUsahaActivity.class));
                                finish();

                            } else {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        progressDialog.dismiss();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_usaha", String.valueOf(id_usaha));
                params.put("nama", nama);
                params.put("alamat", alamat);
                params.put("no_telp", telepon);
                params.put("tahun_data", tahun);
                params.put("latitude", latitude);
                params.put("longitude", longitude);
                params.put("peg_lk", pegPria);
                params.put("peg_pr", pegWanita);
                params.put("deskripsi", deskripsi);
                params.put("no_izin_usaha", noIzinUsaha);
                params.put("id_desa", desa);
                params.put("id_omset", omset);
                params.put("id_aset", aset);
                params.put("id_pelaku", userId);
                params.put("id_izin_usaha", izinUsaha);
                params.put("id_jenis_usaha", jenisUsaha);
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        enableMyLocation();

        center = new LatLng(-4.915966, 105.051816);
        cameraPosition = new CameraPosition.Builder().target(center).zoom(5).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

//        mMap.addMarker(new MarkerOptions().position(center)
//                .title("Set Lokasi Usaha")
//                .snippet("Tekan lama dan geser marker")
//                .draggable(true)
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_lokasi)));

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                latitude = marker.getPosition().latitude;
                longitude = marker.getPosition().longitude;

                et_latitude.setText(String.valueOf(latitude));
                et_longitude.setText(String.valueOf(longitude));
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                latitude = marker.getPosition().latitude;
                longitude = marker.getPosition().longitude;

                et_latitude.setText(String.valueOf(latitude));
                et_longitude.setText(String.valueOf(longitude));
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                latitude = marker.getPosition().latitude;
                longitude = marker.getPosition().longitude;

                et_latitude.setText(String.valueOf(latitude));
                et_longitude.setText(String.valueOf(longitude));
            }
        });
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission((AppCompatActivity) this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    private void loadData(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_DETAIL_USAHA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                            if(obj.getBoolean("success")){
                                detail = obj.getJSONObject("message");

                                et_nama.setText(detail.getString("nama"));
                                et_alamat.setText(detail.getString("alamat"));
                                et_telepon.setText(detail.getString("no_telp"));
                                et_noIzinUsaha.setText(detail.getString("no_izin_usaha"));
                                et_tahun.setText(detail.getString("tahun_data"));
                                peglk.setText(detail.getString("peg_lk"));
                                pegpr.setText(detail.getString("peg_pr"));
                                et_deskripsi.setText(detail.getString("deskripsi"));
                                et_latitude.setText(detail.getString("latitude"));
                                et_longitude.setText(detail.getString("longitude"));

                                mMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(detail.getDouble("latitude"), detail.getDouble("longitude")))
                                        .title("Set Lokasi Usaha")
                                        .snippet("Tekan lama dan geser marker")
                                        .draggable(true)
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_lokasi)));

                                try {
                                    for(int i=0; i < adapterOmset.getCount(); i++){
                                        if(detail.getInt("id_omset") == adapterOmset.getItem(i).getOmsetId()){
                                            sp_omset.setSelection(i);
                                            break;
                                        }
                                    }

                                    for(int i=0; i < adapterAset.getCount(); i++){
                                        if(detail.getInt("id_aset") == adapterAset.getItem(i).getAsetId()){
                                            sp_aset.setSelection(i, true);
                                            break;
                                        }
                                    }

                                    for(int i=0; i < adapterIzinUsaha.getCount(); i++){
                                        if(detail.getInt("id_izin_usaha") == adapterIzinUsaha.getItem(i).getIzinUsahaId()){
                                            sp_izin.setSelection(i, true);
                                            break;
                                        }
                                    }


                                }catch (Exception e){

                                }

                                getSektorUsaha();
                                getProvinsi();

                            }else{
                                Toast.makeText(EditUsahaActivity.this, "Agaknya data lo ilang bro",Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_usaha", String.valueOf(id_usaha));
                return params;
            }
        };
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);
    }

    private void getProvinsi(){

        if(provinsiModelList.size() > 0){
            if (kabupatenModelList.size() > 0) {
                if(kecamatanModelList.size() > 0){
                    if(desaModelList.size() > 0){
                        desaModelList.clear();
                        adapterDesa.notifyDataSetChanged();
                    }
                    kecamatanModelList.clear();
                    adapterKecamatan.notifyDataSetChanged();
                }
                kabupatenModelList.clear();
                adapterKabupaten.notifyDataSetChanged();
            }
            provinsiModelList.clear();
            adapterProvinsi.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_PROVINSI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            int defaultPro = 0;
                            for(int i = 0;i < array.length(); i++){
                                ProvinsiModel provinsiModel = new Gson().fromJson(array.getString(i), ProvinsiModel.class);
                                if(provinsiModel.getProvinsiId() == detail.getInt("id_provinsi") && firstData2){
                                    defaultPro = i;
                                }
                                provinsiModelList.add(provinsiModel);
                            }

                            adapterProvinsi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_provinsi.setAdapter(adapterProvinsi);
                            sp_provinsi.setSelection(defaultPro);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getKabupaten(String provinsi){
        if (kabupatenModelList.size() > 0) {
            if(kecamatanModelList.size() > 0){
                if(desaModelList.size() > 0){
                    desaModelList.clear();
                    adapterDesa.notifyDataSetChanged();
                }
                kecamatanModelList.clear();
                adapterKecamatan.notifyDataSetChanged();
            }
            kabupatenModelList.clear();
            adapterKabupaten.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_KABUPATEN + provinsi,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            int defaultPro = 0;
                            for(int i = 0;i < array.length(); i++){
                                KabupatenModel kabupatenModel = new Gson().fromJson(array.getString(i), KabupatenModel.class);
                                if(kabupatenModel.getKabupatenId() == detail.getInt("id_kabupaten") && firstData2){
                                    defaultPro = i;
                                }
                                kabupatenModelList.add(kabupatenModel);
                            }

                            adapterKabupaten.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_kabupaten.setAdapter(adapterKabupaten);
                            sp_kabupaten.setSelection(defaultPro);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getKecamatan(String kabupaten){
        if (kecamatanModelList.size() > 0) {
            if(desaModelList.size() > 0){
                desaModelList.clear();
                adapterDesa.notifyDataSetChanged();
            }
            kecamatanModelList.clear();
            adapterKecamatan.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_KECAMATAN + kabupaten,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            int defaultPro = 0;
                            for(int i = 0;i < array.length(); i++){
                                KecamatanModel kecamatanModel = new Gson().fromJson(array.getString(i), KecamatanModel.class);
                                if(kecamatanModel.getKecamatanId() == detail.getInt("id_kecamatan") && firstData2){
                                    defaultPro = i;
                                }
                                kecamatanModelList.add(kecamatanModel);
                            }

                            adapterKecamatan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_kecamatan.setAdapter(adapterKecamatan);
                            sp_kecamatan.setSelection(defaultPro);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getDesa(String kecamatan){
        if (desaModelList.size() > 0) {
            desaModelList.clear();
            adapterDesa.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_DESA + kecamatan,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            int defaultPro = 0;
                            for(int i = 0;i < array.length(); i++){
                                DesaModel desaModel = new Gson().fromJson(array.getString(i), DesaModel.class);
                                if(desaModel.getDesaId().equalsIgnoreCase(detail.getString("id_desa")) && firstData2){
                                    defaultPro = i;
                                    firstData2 = false;
                                }
                                desaModelList.add(desaModel);
                            }

                            adapterDesa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_desa.setAdapter(adapterDesa);
                            sp_desa.setSelection(defaultPro);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getSektorUsaha(){
        if (sektorUsahaModelList.size() > 0) {
            if(jenisUsahaModelList.size() > 0){
                jenisUsahaModelList.clear();
                adapterJenisUsaha.notifyDataSetChanged();
            }
            sektorUsahaModelList.clear();
            adapterSektorUsaha.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_SEKTOR_USAHA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            int defaultSektor = 0;
                            for(int i = 0;i < array.length(); i++){
                                SektorUsahaModel sektorUsahaModel = new Gson().fromJson(array.getString(i), SektorUsahaModel.class);
                                if(sektorUsahaModel.getSektorUsahaId() == detail.getInt("id_sektor_usaha") && firstData){
                                    defaultSektor = i;
                                }
                                sektorUsahaModelList.add(sektorUsahaModel);
                            }

                            adapterSektorUsaha.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_sektor.setAdapter(adapterSektorUsaha);
                            sp_sektor.setSelection(defaultSektor);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the reuest to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);
    }

    private void getJenisUsaha(String sektor){
        if (jenisUsahaModelList.size() > 0) {
            jenisUsahaModelList.clear();
            adapterJenisUsaha.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_JENIS_USAHA + sektor,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            int defaultSektor = 0;
                            for(int i = 0;i < array.length(); i++){
                                JenisUsahaModel jenisUsahaModel = new Gson().fromJson(array.getString(i), JenisUsahaModel.class);
                                if(jenisUsahaModel.getJenisUsahaId() == detail.getInt("id_jenis_usaha") && firstData){
                                    defaultSektor = i;
                                    firstData = false;
                                }
                                jenisUsahaModelList.add(jenisUsahaModel);
                            }

                            adapterJenisUsaha.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_jenis.setAdapter(adapterJenisUsaha);
                            sp_jenis.setSelection(defaultSektor);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getAset(){

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_ASET,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length(); i++){
                                AsetModel asetModel = new Gson().fromJson(array.getString(i), AsetModel.class);
                                asetModelList.add(asetModel);
                            }

                            adapterAset = new ArrayAdapter<>(EditUsahaActivity.this, android.R.layout.simple_spinner_item, asetModelList);
                            adapterAset.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_aset.setAdapter(adapterAset);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getOmset(){

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_OMSET,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length(); i++){
                                OmsetModel omsetModel = new Gson().fromJson(array.getString(i), OmsetModel.class);
                                omsetModelList.add(omsetModel);
                            }

                            adapterOmset = new ArrayAdapter<>(EditUsahaActivity.this, android.R.layout.simple_spinner_item, omsetModelList);
                            adapterOmset.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_omset.setAdapter(adapterOmset);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getIzinUsaha(){

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_IZIN_USAHA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length(); i++){
                                IzinUsahaModel izinUsahaModel = new Gson().fromJson(array.getString(i), IzinUsahaModel.class);
                                izinUsahaModelList.add(izinUsahaModel);
                            }

                            adapterIzinUsaha = new ArrayAdapter<>(EditUsahaActivity.this, android.R.layout.simple_spinner_item, izinUsahaModelList);
                            adapterIzinUsaha.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_izin.setAdapter(adapterIzinUsaha);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
