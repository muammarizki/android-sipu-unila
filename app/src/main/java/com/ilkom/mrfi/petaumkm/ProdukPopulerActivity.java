package com.ilkom.mrfi.petaumkm;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.adapters.ProdukPopulerAdapter;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.ProdukPopulerModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muamm on 3/6/2018.
 */

public class ProdukPopulerActivity extends AppCompatActivity {

    RecyclerView rv_produk;
    List<ProdukPopulerModel> produkPopulerModels;
    ProdukPopulerAdapter produkPopulerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produk_populer);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        int columns = 2;
        rv_produk = findViewById(R.id.rv_list_produk);
        rv_produk.setLayoutManager(new GridLayoutManager(this, columns));
        produkPopulerModels = new ArrayList<>();
        produkPopulerAdapter = new ProdukPopulerAdapter(ProdukPopulerActivity.this);

        getProduk();
    }

    private void getProduk(){
        if(produkPopulerModels.size()>0){
            produkPopulerModels.clear();
            produkPopulerAdapter.clearModel();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_GET_PRODUK_POPULER_ALL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length();i++){
                                JSONObject o = array.getJSONObject(i);
                                produkPopulerModels.add(new ProdukPopulerModel(
                                        o.getInt("id"),
                                        o.getString("nama"),
                                        o.getString("harga"),
                                        o.getString("thumbnail"),
                                        o.getInt("pengunjung"),
                                        o.getString("usaha"),
                                        o.getString("kota")
                                ));
                            }

                            produkPopulerAdapter.setProdukModels(produkPopulerModels);
                            rv_produk.setAdapter(produkPopulerAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ProdukPopulerActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(ProdukPopulerActivity.this).add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Cari Produk Terpopuler...");
        searchView.setIconifiedByDefault(false);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                final List<ProdukPopulerModel> filteredModelList = filter(produkPopulerModels, s);

                produkPopulerAdapter.setFilter(filteredModelList);
                return false;
            }
        });

        return true;
    }

    private List<ProdukPopulerModel> filter(List<ProdukPopulerModel> models, String query) {
        query = query.toLowerCase();final List<ProdukPopulerModel> filteredModelList = new ArrayList<>();
        for (ProdukPopulerModel model : models) {
            final String text = model.getNama().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        produkPopulerModels.clear();
        getProduk();
    }
}
