package com.ilkom.mrfi.petaumkm.models;

/**
 * Created by muamm on 3/8/2018.
 */

public class WishlistModel {

    String id;
    int id_produk;
    String thumbnail;
    String nama;
    String harga;
    int pengunjung;
    String usaha;
    String kota;
    int id_user;

    public WishlistModel(String id, int id_produk, String thumbnail, String nama, String harga, int pengunjung, String usaha, String kota, int id_user) {
        this.id = id;
        this.id_produk = id_produk;
        this.thumbnail = thumbnail;
        this.nama = nama;
        this.harga = harga;
        this.pengunjung = pengunjung;
        this.usaha = usaha;
        this.kota = kota;
        this.id_user = id_user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getId_produk() {
        return id_produk;
    }

    public void setId_produk(int id_produk) {
        this.id_produk = id_produk;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public int getPengunjung() {
        return pengunjung;
    }

    public void setPengunjung(int pengunjung) {
        this.pengunjung = pengunjung;
    }

    public String getUsaha() {
        return usaha;
    }

    public void setUsaha(String usaha) {
        this.usaha = usaha;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }
}
