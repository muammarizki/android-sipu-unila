package com.ilkom.mrfi.petaumkm;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.helpers.VolleySingleton;
import com.ilkom.mrfi.petaumkm.models.UserModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by muamm on 3/11/2018.
 */

public class EditPasswordActivity extends AppCompatActivity {

    EditText et_password, et_password2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_password);
        setTitle("Ganti Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        et_password = findViewById(R.id.et_password);
        et_password2 = findViewById(R.id.et_password2);
    }

    private void simpanData(){

        final UserModel user = SharedPrefManager.getInstance(this).getUser();

        final String password = et_password.getText().toString().trim();
        final String password2 = et_password2.getText().toString().trim();

        if (!isMatch(password, password2)) {
            et_password2.setError("Password tidak sama!");
            et_password2.requestFocus();
            return;
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_EDIT_PASSWORD,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject obj = new JSONObject(response);

                            if (obj.getBoolean("success")) {

                                SharedPrefManager.getInstance(getApplicationContext()).userLogin(user);

                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                                finish();
//
                            } else {
                                Toast.makeText(getApplicationContext(), "Gagal menyimpan!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", String.valueOf(user.getId()));
                params.put("password", password);
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }

    private boolean isMatch(String password, String password2){
        return password.equals(password2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.simpan, menu);
        MenuItem item = menu.findItem(R.id.menu_simpan);
        item.setIcon(R.drawable.ic_simpan).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case  R.id.menu_simpan:
                simpanData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
