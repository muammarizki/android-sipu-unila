package com.ilkom.mrfi.petaumkm.models;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by muamm on 2/14/2018.
 */

public class MarkerModel {
    int id;
    String nama;
    int aset;
    String deskripsi;
    String telepon;
    String alamat;
    LatLng latlng;

    public int getAset() {
        return aset;
    }

    public void setAset(int aset) {
        this.aset = aset;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public LatLng getLatlng() {
        return latlng;
    }

    public void setLatlng(LatLng latlng) {
        this.latlng = latlng;
    }
}
