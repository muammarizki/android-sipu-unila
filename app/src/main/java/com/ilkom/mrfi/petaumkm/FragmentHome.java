package com.ilkom.mrfi.petaumkm;

import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.adapters.ProdukBaruAdapter;
import com.ilkom.mrfi.petaumkm.adapters.ProdukPopulerAdapter;
import com.ilkom.mrfi.petaumkm.adapters.UsahaLimitAdapter;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.ProdukBaruModel;
import com.ilkom.mrfi.petaumkm.models.ProdukPopulerModel;
import com.ilkom.mrfi.petaumkm.models.UsahaLimitModel;
import com.novoda.merlin.Merlin;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muamm on 12/9/2017.
 */

public class FragmentHome extends Fragment {

    RecyclerView rv_usaha, rv_produkbaru, rv_produkpopuler;
    List<UsahaLimitModel> usahaModels;
    List<ProdukBaruModel> produkModels;
    List<ProdukPopulerModel> produkPopulerModels;

    ProdukBaruAdapter produkBaruAdapter;
    ProdukPopulerAdapter produkPopulerAdapter;
    UsahaLimitAdapter usahaLimitAdapter;

    RelativeLayout produk_baru, produk_populer, umkm;
    LinearLayout home, errorView;

    SwipeRefreshLayout swipeRefreshLayout;
    CoordinatorLayout coordinatorLayout;
    Snackbar snackbar;

    public FragmentHome() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.activity_fragment_home, container, false);


        coordinatorLayout = rootView.findViewById(R.id.coordinator);
        errorView = rootView.findViewById(R.id.errorView);
        home = rootView.findViewById(R.id.home);

        if (!SharedPrefManager.getInstance(getActivity()).isLoggedIn()) {
            getActivity().finish();
            startActivity(new Intent(getActivity(), LoginActivity.class));
        }

        rv_usaha = rootView.findViewById(R.id.rv_umkm);
        rv_usaha.setLayoutManager(new LinearLayoutManager(getContext()));
        usahaLimitAdapter = new UsahaLimitAdapter(getContext());
        usahaModels = new ArrayList<>();

        rv_produkbaru = rootView.findViewById(R.id.rv_produk_baru);
        rv_produkbaru.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        produkBaruAdapter = new ProdukBaruAdapter(getContext());
        produkModels = new ArrayList<>();

        rv_produkpopuler = rootView.findViewById(R.id.rv_produk_populer);
        rv_produkpopuler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        produkPopulerAdapter = new ProdukPopulerAdapter(getContext());
        produkPopulerModels = new ArrayList<>();

        produk_baru = rootView.findViewById(R.id.layout_produk_baru);
        produk_baru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ProdukBaruActivity.class));
            }
        });

        produk_populer = rootView.findViewById(R.id.layout_produk_populer);
        produk_populer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ProdukPopulerActivity.class));
            }
        });

        umkm = rootView.findViewById(R.id.layout_umkm);
        umkm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), UsahaActivity.class));
            }
        });

        snackbar = Snackbar
                .make(coordinatorLayout, "Tidak Ada Koneksi", Snackbar.LENGTH_INDEFINITE)
                .setAction("TUTUP", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        snackbar.dismiss();
                    }
                });

        swipeRefreshLayout = rootView.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!isNetworkConnected()){
                    errorView.setVisibility(View.VISIBLE);
                    home.setVisibility(View.GONE);

                    snackbar.show();
                    swipeRefreshLayout.setRefreshing(false);
                }else {
                    errorView.setVisibility(View.GONE);
                    home.setVisibility(View.VISIBLE);

                    snackbar.dismiss();
                    produkModels.clear();
                    produkPopulerModels.clear();
                    usahaModels.clear();
                    getProduk();
                    getProdukPopuler();
                    getUsaha();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        if(!isNetworkConnected()){
            errorView.setVisibility(View.VISIBLE);
            home.setVisibility(View.GONE);
            snackbar.show();
        }else {
            snackbar.dismiss();
            home.setVisibility(View.VISIBLE);
            errorView.setVisibility(View.GONE);
        }

        return rootView;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void getUsaha(){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_GET_USAHA_LIMIT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length();i++){
                                JSONObject o = array.getJSONObject(i);
                                usahaModels.add(new UsahaLimitModel(
                                        o.getInt("id"),
                                        o.getString("nama"),
                                        o.getString("kota"),
                                        o.getInt("rating"),
                                        o.getInt("jumlah")
                                ));
                            }

                            usahaLimitAdapter.setUsahaModels(usahaModels);
                            rv_usaha.setAdapter(usahaLimitAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(getContext(), error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(getContext()).add(stringRequest);
    }

    private void getProdukPopuler(){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_GET_PRODUK_LIMIT_POPULER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length();i++){
                                JSONObject o = array.getJSONObject(i);
                                produkPopulerModels.add(new ProdukPopulerModel(
                                        o.getInt("id"),
                                        o.getString("nama"),
                                        o.getString("harga"),
                                        o.getString("thumbnail"),
                                        o.getInt("pengunjung"),
                                        o.getString("usaha"),
                                        o.getString("kota")
                                ));
                            }

                            produkPopulerAdapter.setProdukModels(produkPopulerModels);
                            rv_produkpopuler.setAdapter(produkPopulerAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(getContext(), error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(getContext()).add(stringRequest);

    }

    private void getProduk(){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_GET_PRODUK_LIMIT_BARU,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length();i++){
                                JSONObject o = array.getJSONObject(i);
                                produkModels.add(new ProdukBaruModel(
                                        o.getInt("id"),
                                        o.getString("nama"),
                                        o.getString("harga"),
                                        o.getString("thumbnail"),
                                        o.getInt("pengunjung"),
                                        o.getString("usaha"),
                                        o.getString("kota")
                                ));
                            }

                            produkBaruAdapter.setProdukModels(produkModels);
                            rv_produkbaru.setAdapter(produkBaruAdapter);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(getContext(), error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });

        Volley.newRequestQueue(getContext()).add(stringRequest);
    }

//    public void refresh() {
//        Intent intent = getActivity().getIntent();
//        getActivity().overridePendingTransition(0, 0);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//        getActivity().finish();
//        getActivity().overridePendingTransition(0, 0);
//        startActivity(intent);
//    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.pencarian, menu);
        menu.findItem(R.id.search);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.search:
                startActivity(new Intent(getActivity(), PencarianActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onResume(){
        super.onResume();
        ((MainActivity) getActivity())
                .setActionBarTitle("Beranda");

        produkModels.clear();
        usahaModels.clear();
        produkPopulerModels.clear();

        getUsaha();
        getProdukPopuler();
        getProduk();
    }
}

