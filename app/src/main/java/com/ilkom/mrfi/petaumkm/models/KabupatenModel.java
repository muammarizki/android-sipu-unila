package com.ilkom.mrfi.petaumkm.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by muamm on 1/27/2018.
 */

public class KabupatenModel {

    @SerializedName("id")
    private int kabupatenId;

    @SerializedName("id_provinsi")
    private int provinsiId;

    @SerializedName("nama")
    private String namaKabupaten;

    public int getKabupatenId() {
        return kabupatenId;
    }

    public void setKabupatenId(int kabupatenId) {
        this.kabupatenId = kabupatenId;
    }

    public int getProvinsiId() {
        return provinsiId;
    }

    public void setProvinsiId(int provinsiId) {
        this.provinsiId = provinsiId;
    }

    public String getNamaKabupaten() {
        return namaKabupaten;
    }

    public void setNamaKabupaten(String namaKabupaten) {
        this.namaKabupaten = namaKabupaten;
    }

    @Override
    public String toString(){
        return namaKabupaten;
    }
}
