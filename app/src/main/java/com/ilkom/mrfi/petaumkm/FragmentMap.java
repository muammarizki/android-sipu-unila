package com.ilkom.mrfi.petaumkm;

import android.Manifest;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ilkom.mrfi.petaumkm.adapters.CustomWindowAdapter;
import com.ilkom.mrfi.petaumkm.helpers.PermissionUtils;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.InfoWindowData;
import com.ilkom.mrfi.petaumkm.models.MarkerModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muamm on 12/9/2017.
 */

public class FragmentMap extends Fragment implements OnMapReadyCallback, GoogleMap.OnMyLocationClickListener,
        GoogleMap.OnMyLocationButtonClickListener, ActivityCompat.OnRequestPermissionsResultCallback {

    private GoogleMap mMap;
    private View view;
    MarkerOptions markerOptions = new MarkerOptions();
    CameraPosition cameraPosition;
    LatLng center;
    private boolean mPermissionDenied = false;

    List<MarkerModel> markerModels;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    CoordinatorLayout coordinatorLayout;
    Snackbar snackbar;
    private boolean internetConnected = true;
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    PlaceAutocompleteFragment placeAutoComplete;

    public FragmentMap() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.activity_fragment_map, container, false);
        setHasOptionsMenu(true);

        if (!SharedPrefManager.getInstance(getActivity()).isLoggedIn()) {
            getActivity().finish();
            startActivity(new Intent(getActivity(), LoginActivity.class));
        }

        coordinatorLayout = view.findViewById(R.id.coordinator);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        markerModels = new ArrayList<>();

        return this.view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        enableMyLocation();

        center = new LatLng(-4.915966, 105.051816);
        cameraPosition = new CameraPosition.Builder().target(center).zoom(8).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        getMarkers();
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission((AppCompatActivity) getActivity(), LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(getActivity(), "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(getActivity(), "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getActivity().getSupportFragmentManager(), "dialog");
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


    private void addMarker(final LatLng latLng, final String nama, final String deskripsi, final String telepon, final int omset, final String alamat){
        markerOptions.position(latLng);
        markerOptions.title(nama);
        if(omset == 1){
            markerOptions.icon(bitmapDescriptorFromVector(getActivity(), R.drawable.ic_mikro));
        }
        if(omset == 2){
            markerOptions.icon(bitmapDescriptorFromVector(getActivity(), R.drawable.ic_kecil));
        }
        if(omset == 3){
            markerOptions.icon(bitmapDescriptorFromVector(getActivity(), R.drawable.ic_menengah));
        }
        markerOptions.snippet(deskripsi);

        InfoWindowData infoWindowData = new InfoWindowData();
        infoWindowData.setAlamat(alamat);
        infoWindowData.setTelepon(telepon);

        CustomWindowAdapter customWindowAdapter = new CustomWindowAdapter(getActivity());
        mMap.setInfoWindowAdapter(customWindowAdapter);
        Marker m = mMap.addMarker(markerOptions);
        m.setTag(infoWindowData);
//        m.showInfoWindow();

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                for(MarkerModel markerModel : markerModels){
                    if(markerModel.getLatlng().latitude == marker.getPosition().latitude && markerModel.getLatlng().longitude == marker.getPosition().longitude){
                        Intent i = new Intent(getActivity(), DetailUsahaActivity.class);
                        i.putExtra("id", markerModel.getId());
                        getActivity().startActivity(i);
                        break;
                    }
                }

            }
        });
    }

    private void getMarkers(){
        final ProgressDialog progressDialog = ProgressDialog.show(getContext(), "", "Mengambil data UMKM...", false, false);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_GET_USAHA_MARKER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    JSONArray array = obj.getJSONArray("message");

                    for(int i=0; i < array.length(); i++){
                        JSONObject objs = array.getJSONObject(i);
                        int id = objs.getInt("id");
                        String nama = objs.getString("nama");
                        int aset = objs.getInt("id_aset");
                        String alamat = objs.getString("alamat");
                        String deskripsi = objs.getString("deskripsi");
                        String telepon = objs.getString("telepon");
                        LatLng latLng = new LatLng(Double.parseDouble(objs.getString("latitude")), Double.parseDouble(objs.getString("longitude")));
                        addMarker(latLng, nama, deskripsi, telepon, aset, alamat);

                        MarkerModel markerModel = new MarkerModel();
                        markerModel.setId(id);
                        markerModel.setNama(nama);
                        markerModel.setAset(aset);
                        markerModel.setDeskripsi(deskripsi);
                        markerModel.setAlamat(alamat);
                        markerModel.setTelepon(telepon);
                        markerModel.setLatlng(latLng);
                        markerModels.add(markerModel);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                progressDialog.dismiss();
            }
        }, new Response.ErrorListener(){
           public void onErrorResponse(VolleyError error){
               progressDialog.dismiss();
               Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
           }
        });
        Volley.newRequestQueue(getActivity()).add(stringRequest);
    }

    public void onResume(){
        super.onResume();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
        registerInternetCheckReceiver();
        // Set title bar
        ((MainActivity) getActivity())
                .setActionBarTitle("Peta Persebaran UMKM");
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    private void registerInternetCheckReceiver() {
        IntentFilter internetFilter = new IntentFilter();
        internetFilter.addAction("android.net.wifi.STATE_CHANGE");
        internetFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        getActivity().registerReceiver(broadcastReceiver, internetFilter);
    }

    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = getConnectivityStatusString(context);
            setSnackbarMessage(status,false);
        }
    };

    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static String getConnectivityStatusString(Context context) {
        int conn = getConnectivityStatus(context);
        String status = null;
        if (conn == TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == TYPE_MOBILE) {
            status = "Mobile data enabled";
        } else if (conn == TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet";
        }
        return status;
    }

    private void setSnackbarMessage(String status,boolean showBar) {
        String internetStatus="";
        if(status.equalsIgnoreCase("Wifi enabled")||status.equalsIgnoreCase("Mobile data enabled")){
            internetStatus="Koneksi Tersedia";
            snackbar = Snackbar
                    .make(coordinatorLayout, internetStatus, Snackbar.LENGTH_INDEFINITE)
                    .setAction("MUAT ULANG", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            refresh();
                        }
                    });
        }else {
            internetStatus="Tidak Ada Koneksi";
            snackbar = Snackbar
                    .make(coordinatorLayout, internetStatus, Snackbar.LENGTH_INDEFINITE)
                    .setAction("TUTUP", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            snackbar.dismiss();
                        }
                    });
        }

        // Changing action button primaryText color
        View sbView = snackbar.getView();
        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        if(internetStatus.equalsIgnoreCase("Tidak ada koneksi")){
            if(internetConnected){
                snackbar.show();
                internetConnected=false;
            }
        }else{
            if(!internetConnected){
                internetConnected=true;
                snackbar.show();
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        menu.findItem(R.id.search);

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setQueryHint("Cari UMKM...");
        searchView.setIconifiedByDefault(false);

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                for(MarkerModel m : markerModels) {
                    if(m.getNama().toLowerCase().equalsIgnoreCase(s)) {
                        LatLng latLng = m.getLatlng();
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18));
                        break;
                    }
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                return false;
            }
        });
    }

}