package com.ilkom.mrfi.petaumkm.helpers;

/**
 * Created by muamm on 12/27/2017.
 */

public class URLs {
//    private static final String ROOT_URL = "http://192.168.137.1/sipu-unila/public/api/";
    private static final String ROOT_URL = "http://sipu.capung.tech/api/";

    //URL API USER
    public static final String URL_LOGIN = ROOT_URL + "user/login";
    public static final String URL_REGISTER = ROOT_URL + "user/registrasi";

    public static final String URL_DETAIL_USER= ROOT_URL + "user/detail";
    public static final String URL_EDIT_PASSWORD= ROOT_URL + "user/password/edit";

    public static final String URL_TAMBAH_BIODATA = ROOT_URL + "user/biodata/tambah";
    public static final String URL_EDIT_BIODATA = ROOT_URL + "user/biodata/edit";
    public static final String URL_GANTI_FOTO_PROFIL = ROOT_URL + "user/biodata/foto-profil/edit";
    public static final String URL_DETAIL_BIODATA = ROOT_URL + "user/biodata/detail";
    public static final String URL_DETAIL_BIODATA_PELANGGAN = ROOT_URL + "user/biodata/pelanggan/detail";
    public static final String URL_EDIT_BIODATA_PELANGGAN = ROOT_URL + "user/biodata/pelanggan/edit";

    //URL API WILAYAH
    public static final String URL_KABUPATEN= ROOT_URL + "wilayah/kabupaten/";
    public static final String URL_PROVINSI= ROOT_URL + "wilayah/provinsi";
    public static final String URL_KECAMATAN= ROOT_URL + "wilayah/kecamatan/";
    public static final String URL_DESA= ROOT_URL + "wilayah/desa/";

    //URL API USAHA
    public static final String URL_TAMBAH_USAHA= ROOT_URL + "usaha/tambah";
    public static final String URL_DETAIL_USAHA= ROOT_URL + "usaha/detail";
    public static final String URL_HAPUS_USAHA= ROOT_URL + "usaha/hapus";
    public static final String URL_EDIT_USAHA= ROOT_URL + "usaha/edit";
    public static final String URL_INDEX_USAHA= ROOT_URL + "usaha/index";

    public static final String URL_GET_USAHA_ALL= ROOT_URL + "usaha/get-usaha-all";
    public static final String URL_GET_USAHA_MARKER= ROOT_URL + "usaha/get-usaha-marker";
    public static final String URL_GET_USAHA_LIMIT= ROOT_URL + "usaha/get-usaha-limit";

    public static final String URL_IZIN_USAHA = ROOT_URL + "usaha/izin-usaha";
    public static final String URL_JENIS_USAHA = ROOT_URL + "usaha/jenis-usaha/";
    public static final String URL_SEKTOR_USAHA = ROOT_URL + "usaha/sektor-usaha";
    public static final String URL_OMSET = ROOT_URL + "usaha/omset";
    public static final String URL_ASET = ROOT_URL + "usaha/aset";

    //URL API FEEDBACK
    public static final String URL_FEEDBACK_USAHA= ROOT_URL + "usaha/tambah-feedback";
    public static final String URL_GET_FEEDBACK_USAHA_ALL = ROOT_URL + "usaha/get-feedback-all/";
    public static final String URL_GET_FEEDBACK_USAHA_LIMIT = ROOT_URL + "usaha/get-feedback-limit/";

    //URL API PRODUK
    public static final String URL_TAMBAH_PRODUK= ROOT_URL + "produk/tambah";
    public static final String URL_HAPUS_PRODUK= ROOT_URL + "produk/hapus";
    public static final String URL_EDIT_PRODUK= ROOT_URL + "produk/edit/";
    public static final String URL_INDEX_PRODUK = ROOT_URL + "produk/index";
    public static final String URL_DETAIL_PRODUK = ROOT_URL + "produk/detail";

    public static final String URL_TAMBAH_PRODUK_HARAPAN = ROOT_URL + "produk/produk-harapan/tambah";
    public static final String URL_HAPUS_PRODUK_HARAPAN = ROOT_URL + "produk/produk-harapan/hapus";
    public static final String URL_INDEX_PRODUK_HARAPAN = ROOT_URL + "produk/produk-harapan/index";

    public static final String URL_GET_PRODUK_POPULER_ALL = ROOT_URL + "produk/get-produk-populer-all";
    public static final String URL_GET_PRODUK_BARU_ALL = ROOT_URL + "produk/get-produk-baru-all";
    public static final String URL_GET_PRODUK_ALL = ROOT_URL + "produk/get-produk-all";
    public static final String URL_GET_PRODUK_LIMIT_BARU = ROOT_URL + "produk/get-produk-limit-baru";
    public static final String URL_GET_PRODUK_LIMIT_POPULER = ROOT_URL + "produk/get-produk-limit-populer";
    public static final String URL_GET_PRODUK_USAHA_LIMIT = ROOT_URL + "produk/get-produk-usaha-limit";
    public static final String URL_TAMBAH_PENGUNJUNG = ROOT_URL + "produk/tambah-pengunjung";



}