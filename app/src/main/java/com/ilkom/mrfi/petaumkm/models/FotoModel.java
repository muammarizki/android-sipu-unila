package com.ilkom.mrfi.petaumkm.models;

/**
 * Created by muamm on 4/1/2018.
 */

public class FotoModel {

    String id;
    String foto;

    public FotoModel(String id, String foto) {
        this.id = id;
        this.foto = foto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
