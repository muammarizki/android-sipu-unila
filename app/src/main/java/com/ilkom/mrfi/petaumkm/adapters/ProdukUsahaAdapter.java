package com.ilkom.mrfi.petaumkm.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.DetailUsahaActivity;
import com.ilkom.mrfi.petaumkm.KelolaUsahaActivity;
import com.ilkom.mrfi.petaumkm.ListProdukActivity;
import com.ilkom.mrfi.petaumkm.R;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.ProdukUsahaModel;
import com.ilkom.mrfi.petaumkm.models.UsahaModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muamm on 1/29/2018.
 */

public class ProdukUsahaAdapter extends RecyclerView.Adapter<ProdukUsahaAdapter.ProdukUsahaViewHolder> {
    private Context context;
    private List<ProdukUsahaModel> produkUsahaModels ;

    public ProdukUsahaAdapter(Context context){
        this.context = context;
        this.produkUsahaModels = new ArrayList<>();
    }

    public void setProdukModels(List<ProdukUsahaModel> produkModels) {
        this.produkUsahaModels = produkModels;
        notifyDataSetChanged();
    }

    public void setFilter(List<ProdukUsahaModel> produkModels) {
        produkUsahaModels = new ArrayList<>();
        produkUsahaModels.addAll(produkModels);
        notifyDataSetChanged();
    }

    public void clearModel(){
        notifyDataSetChanged();
        produkUsahaModels.clear();
    }

    public ProdukUsahaAdapter.ProdukUsahaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.usaha_produk_item, null);
        return new ProdukUsahaAdapter.ProdukUsahaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProdukUsahaAdapter.ProdukUsahaViewHolder holder, final int position) {
        ProdukUsahaModel usaha = produkUsahaModels.get(position);

        holder.tv_nama.setText(usaha.getNama());
        holder.tv_deskripsi.setText(usaha.getDeskripsi());

        holder.cv_usaha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ListProdukActivity.class);
                intent.putExtra("nama", produkUsahaModels.get(position).getNama());
                intent.putExtra("id", produkUsahaModels.get(position).getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return produkUsahaModels.size();
    }

    class ProdukUsahaViewHolder extends RecyclerView.ViewHolder {

        TextView tv_nama, tv_deskripsi;
        private CardView cv_usaha;

        public ProdukUsahaViewHolder(View itemView) {
            super(itemView);

            tv_nama = itemView.findViewById(R.id.tv_nama);
            tv_deskripsi = itemView.findViewById(R.id.tv_deskripsi);
            cv_usaha = itemView.findViewById(R.id.cv_usaha);
        }
    }

}
