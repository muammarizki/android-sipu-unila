package com.ilkom.mrfi.petaumkm.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ilkom.mrfi.petaumkm.DetailUsahaActivity;
import com.ilkom.mrfi.petaumkm.R;
import com.ilkom.mrfi.petaumkm.models.UsahaLimitModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muamm on 2/7/2018.
 */

public class UsahaLimitAdapter extends RecyclerView.Adapter<UsahaLimitAdapter.UsahaViewHolder> {
    private Context context;
    private List<UsahaLimitModel> models;

    public UsahaLimitAdapter(Context context){
        this.context = context;
        this.models = new ArrayList<>();
    }

    public UsahaLimitAdapter.UsahaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.usaha_item_horizontal, null);
        return new UsahaLimitAdapter.UsahaViewHolder(view);
    }

    public void setUsahaModels(List<UsahaLimitModel> usahaModels) {
        this.models = usahaModels;
        notifyDataSetChanged();
    }

    public void setFilter(List<UsahaLimitModel> usahaModels) {
        models = new ArrayList<>();
        models.addAll(usahaModels);
        notifyDataSetChanged();
    }

    public void clearModel(){
        models.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final UsahaLimitAdapter.UsahaViewHolder holder, final int position) {
            UsahaLimitModel usaha = models.get(position);

            holder.tv_kota.setText(usaha.getKota());
            holder.tv_jumlah.setText("(" + String.valueOf(usaha.getJumlah()) + ")");
            holder.rating.setRating(usaha.getRating());
            holder.tv_nama.setText(usaha.getNama());
            holder.cv_usaha.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DetailUsahaActivity.class);
                    intent.putExtra("id", models.get(position).getId());
                    context.startActivity(intent);
                }
            });
    }

    @Override
    public int getItemCount() {
            return models.size();
            }

    class UsahaViewHolder extends RecyclerView.ViewHolder {

        TextView tv_nama, tv_kota, tv_jumlah;
        CardView cv_usaha;
        RatingBar rating;

        public UsahaViewHolder(View itemView) {
            super(itemView);

            tv_nama = itemView.findViewById(R.id.tv_nama);
            cv_usaha = itemView.findViewById(R.id.cv_usaha);
            tv_kota = itemView.findViewById(R.id.lokasi_umkm);
            tv_jumlah = itemView.findViewById(R.id.tv_jumlah);
            rating = itemView.findViewById(R.id.rb_usaha);
        }
    }

}

