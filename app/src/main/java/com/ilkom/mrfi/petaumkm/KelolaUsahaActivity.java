package com.ilkom.mrfi.petaumkm;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.helpers.EmptyRecyclerView;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.UserModel;
import com.ilkom.mrfi.petaumkm.adapters.UsahaAdapter;
import com.ilkom.mrfi.petaumkm.models.UsahaModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by muamm on 12/29/2017.
 */

public class KelolaUsahaActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<UsahaModel> usahaModelList;
    UsahaAdapter usahaAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    LinearLayout emptyView, errorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kelola_usaha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        emptyView = findViewById(R.id.emptyView);
        errorView = findViewById(R.id.errorView);

        recyclerView = findViewById(R.id.rv_usaha);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        usahaAdapter = new UsahaAdapter(KelolaUsahaActivity.this);
        usahaModelList = new ArrayList<>();

        swipeRefreshLayout = findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!isNetworkConnected()){
                    errorView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);

                    swipeRefreshLayout.setRefreshing(false);
                }else {
                    errorView.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    usahaModelList.clear();
                    loadRecyclerViewData();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        if(!isNetworkConnected()){
            errorView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }else{
            errorView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

        loadRecyclerViewData();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void loadRecyclerViewData(){
        if(usahaModelList.size()>0){
            usahaModelList.clear();
            usahaAdapter.clearModel();
        }

        final UserModel user = SharedPrefManager.getInstance(this).getUser();

        final ProgressDialog progressDialog = ProgressDialog.show(this, "", "Mengambil data usaha...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_INDEX_USAHA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                            if(obj.getBoolean("success")){
                                JSONArray array = obj.getJSONArray("message");
                                for(int i = 0;i < array.length();i++){
                                    JSONObject o = array.getJSONObject(i);
                                    usahaModelList.add(new UsahaModel(
                                            o.getInt("id"),
                                            o.getString("nama"),
                                            o.getString("deskripsi"),
                                            o.getString("alamat"),
                                            o.getInt("no_telp"),
                                            o.getInt("tahun_data"),
                                            o.getDouble("latitude"),
                                            o.getDouble("longitude"),
                                            o.getInt("id_pelaku"),
                                            o.getInt("id_jenis_usaha"),
                                            o.getInt("id_omset"),
                                            o.getInt("id_izin_usaha"),
                                            o.getInt("peg_lk"),
                                            o.getInt("peg_pr"),
                                            o.getString("no_izin_usaha"),
                                            o.getInt("id_desa")

                                    ));
                                }

                                usahaAdapter.setUsahaModels(usahaModelList);
                                recyclerView.setAdapter(usahaAdapter);

                            }else{
                                Toast.makeText(KelolaUsahaActivity.this, obj.getString("message"), Toast.LENGTH_LONG).show();
                            }

                            if(usahaAdapter.getItemCount() == 0){
                                recyclerView.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                                errorView.setVisibility(View.GONE);
                            }else{
                                recyclerView.setVisibility(View.VISIBLE);
                                emptyView.setVisibility(View.GONE);
                                errorView.setVisibility(View.GONE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(KelolaUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_user", String.valueOf(user.getId()));
                return params;
            }
        };
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.tambah, menu);
        menu.findItem(R.id.menu_tambah).setIcon(R.drawable.ic_add);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Cari Usahamu...");
        searchView.setIconifiedByDefault(false);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                final List<UsahaModel> filteredModelList = filter(usahaModelList, s);

                usahaAdapter.setFilter(filteredModelList);
                return false;
            }
        });

        return true;
    }

    private List<UsahaModel> filter(List<UsahaModel> models, String query) {
        query = query.toLowerCase();final List<UsahaModel> filteredModelList = new ArrayList<>();
        for (UsahaModel model : models) {
            final String text = model.getNama().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.menu_tambah:
                usaha();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void refresh() {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    public void usaha(){
        Intent usaha = new Intent(this, TambahUsahaActivity.class);
        startActivity(usaha);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        loadRecyclerViewData(user.getId());
//    }

    @Override
    protected void onRestart(){
        super.onRestart();

        usahaModelList.clear();
        loadRecyclerViewData();
    }
}