package com.ilkom.mrfi.petaumkm.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by muamm on 3/15/2018.
 */

public class SektorUsahaModel {

    @SerializedName("id")
    private int sektorUsahaId;

    @SerializedName("nama")
    private String namaSektorUsaha;

    public int getSektorUsahaId() {
        return sektorUsahaId;
    }

    public void setSektorUsahaId(int sektorUsahaId) {
        this.sektorUsahaId = sektorUsahaId;
    }

    public String getNamaSektorUsaha() {
        return namaSektorUsaha;
    }

    public void setNamaSektorUsaha(String namaSektorUsaha) {
        this.namaSektorUsaha = namaSektorUsaha;
    }

    @Override
    public String toString(){
        return namaSektorUsaha;
    }

}
