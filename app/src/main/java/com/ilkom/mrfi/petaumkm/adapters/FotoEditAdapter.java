package com.ilkom.mrfi.petaumkm.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ilkom.mrfi.petaumkm.R;
import com.ilkom.mrfi.petaumkm.models.FotoModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by muamm on 4/1/2018.
 */

public class FotoEditAdapter extends RecyclerView.Adapter<FotoEditAdapter.FotoViewHolder> {
    private Context context;
    private List<String> models;
    private List<FotoModel> fotoModels;

    public FotoEditAdapter(Context context){
        this.context = context;
//        this.models = models;
        this.fotoModels = new ArrayList<>();
    }

    public void setModelItem(String path){
        models.add(path);
        notifyDataSetChanged();
    }

    public List<String> getModels() {
        return models;
    }

    public void setProdukModels(List<FotoModel> fotoModelList) {
        this.fotoModels = fotoModelList;
        notifyDataSetChanged();
    }

    public void clearModel(){
        notifyDataSetChanged();
        fotoModels.clear();
    }

    public FotoEditAdapter.FotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.list_foto, null);
        return new FotoEditAdapter.FotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FotoEditAdapter.FotoViewHolder holder, final int position) {

//        String usaha = models.get(position);
//        holder.foto.setDrawingCacheEnabled(true);
//        holder.foto.setImageBitmap(getResizedBitmap(BitmapFactory.decodeFile(usaha), 100));
        holder.hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hapusFoto();
                fotoModels.remove(position);
                notifyDataSetChanged();
            }
        });

        Picasso.with(context)
                .load("http://sipu.capung.tech/api/produk/foto-produk/get/" + fotoModels.get(position).getFoto())
                .into(holder.foto);

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void hapusFoto(){

    }

    @Override
    public int getItemCount() {
        return fotoModels.size();
    }

    class FotoViewHolder extends RecyclerView.ViewHolder {

        ImageView foto, hapus;

        public FotoViewHolder(View itemView) {
            super(itemView);

            foto = itemView.findViewById(R.id.foto_produk);
            hapus = itemView.findViewById(R.id.hapus);
        }
    }
}

