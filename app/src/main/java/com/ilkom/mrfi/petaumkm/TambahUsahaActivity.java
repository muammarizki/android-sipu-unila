package com.ilkom.mrfi.petaumkm;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.ilkom.mrfi.petaumkm.helpers.MapsFragment;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.helpers.VolleySingleton;
import com.ilkom.mrfi.petaumkm.models.AsetModel;
import com.ilkom.mrfi.petaumkm.models.DesaModel;
import com.ilkom.mrfi.petaumkm.models.IzinUsahaModel;
import com.ilkom.mrfi.petaumkm.models.JenisUsahaModel;
import com.ilkom.mrfi.petaumkm.models.KabupatenModel;
import com.ilkom.mrfi.petaumkm.models.KecamatanModel;
import com.ilkom.mrfi.petaumkm.models.MarkerModel;
import com.ilkom.mrfi.petaumkm.models.OmsetModel;
import com.ilkom.mrfi.petaumkm.models.ProvinsiModel;
import com.ilkom.mrfi.petaumkm.models.SektorUsahaModel;
import com.ilkom.mrfi.petaumkm.models.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by muamm on 1/26/2018.
 */

public class TambahUsahaActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;

    ArrayAdapter<ProvinsiModel> adapterProvinsi;
    ArrayAdapter<KabupatenModel> adapterKabupaten;
    ArrayAdapter<KecamatanModel> adapterKecamatan;
    ArrayAdapter<DesaModel> adapterDesa;
    ArrayAdapter<OmsetModel> adapterOmset;
    ArrayAdapter<AsetModel> adapterAset;
    ArrayAdapter<JenisUsahaModel> adapterJenisUsaha;
    ArrayAdapter<IzinUsahaModel> adapterIzinUsaha;
    ArrayAdapter<SektorUsahaModel> adapterSektorUsaha;

    EditText et_nama, et_deskripsi, et_alamat, et_telepon, et_tahun, et_pria, et_wanita, et_noIzinUsaha, et_latitude, et_longitude;
    Button btn_simpan;
    Spinner sp_provinsi, sp_kabupaten, sp_kecamatan, sp_desa, sp_omset, sp_izin, sp_jenis, sp_sektor, sp_aset;

    private List<ProvinsiModel> provinsiModelList = new ArrayList<>();
    private List<KabupatenModel> kabupatenModelList = new ArrayList<>();
    private List<KecamatanModel> kecamatanModelList = new ArrayList<>();
    private List<DesaModel> desaModelList = new ArrayList<>();
    private List<OmsetModel> omsetModelList = new ArrayList<>();
    private List<AsetModel> asetModelList = new ArrayList<>();
    private List<JenisUsahaModel> jenisUsahaModelList = new ArrayList<>();
    private List<IzinUsahaModel> izinUsahaModelList = new ArrayList<>();
    private List<SektorUsahaModel> sektorUsahaModelList = new ArrayList<>();

    ScrollView mScrollView;
    GoogleMap mMap;
    CameraPosition cameraPosition;
    LatLng center;
    double latitude, longitude;
    String desa, omset, izinUsaha, jenisUsaha, sektorUsaha, aset;
//    PlaceAutocompleteFragment placeAutoComplete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_usaha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

//        placeAutoComplete = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete);

        ((MapsFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map)).getMapAsync(this);
        mScrollView = findViewById(R.id.sv_container);

        ((MapsFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map)).setListener(new MapsFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                mScrollView.requestDisallowInterceptTouchEvent(true);
            }
        });

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        sp_provinsi = findViewById(R.id.sp_provinsi);
        getProvinsi();
        sp_provinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String provinsi = String.valueOf(provinsiModelList.get(position).getProvinsiId());
                getKabupaten(provinsi);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_kabupaten = findViewById(R.id.sp_kabupaten);
        sp_kabupaten.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String kabupaten = String.valueOf(kabupatenModelList.get(position).getKabupatenId());
                getKecamatan(kabupaten);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_kecamatan = findViewById(R.id.sp_kecamatan);
        sp_kecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String kecamatan = String.valueOf(kecamatanModelList.get(position).getKecamatanId());
                getDesa(kecamatan);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_desa = findViewById(R.id.sp_desa);
        sp_desa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                desa = String.valueOf(desaModelList.get(position).getDesaId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_omset = findViewById(R.id.sp_omset);
        getOmset();
        sp_omset.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                omset = String.valueOf(omsetModelList.get(position).getOmsetId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_aset = findViewById(R.id.sp_aset);
        getAset();
        sp_aset.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                aset = String.valueOf(asetModelList.get(i).getAsetId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sp_izin = findViewById(R.id.sp_izin_usaha);
        getIzinUsaha();
        sp_izin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                izinUsaha = String.valueOf(izinUsahaModelList.get(position).getIzinUsahaId());

                if(izinUsahaModelList.get(position).getNamaIzinUsaha().toString().equals("Tidak Ada")){
                    et_noIzinUsaha.setEnabled(false);
                }else{
                    et_noIzinUsaha.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_sektor = findViewById(R.id.sp_sektor_usaha);
        getSektorUsaha();
        sp_sektor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                sektorUsaha = String.valueOf(sektorUsahaModelList.get(i).getSektorUsahaId());
                getJenisUsaha(sektorUsaha);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sp_jenis = findViewById(R.id.sp_jenis_usaha);
        sp_jenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                jenisUsaha = String.valueOf(jenisUsahaModelList.get(position).getJenisUsahaId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        et_latitude = findViewById(R.id.et_latitude);
        et_latitude.setEnabled(false);

        et_longitude = findViewById(R.id.et_longitude);
        et_longitude.setEnabled(false);

        et_nama = findViewById(R.id.et_nama);
        et_deskripsi = findViewById(R.id.et_deskripsi);
        et_telepon = findViewById(R.id.et_telepon);
        et_alamat = findViewById(R.id.et_alamat);
        et_noIzinUsaha = findViewById(R.id.et_no_izin_usaha);
        et_pria = findViewById(R.id.et_peglk);
        et_wanita = findViewById(R.id.et_pegpr);
        et_tahun = findViewById(R.id.et_tahun);

        btn_simpan = findViewById(R.id.btn_simpan);
        btn_simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                simpanData();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

//        center = new LatLng(-4.915966, 105.051816);
//        cameraPosition = new CameraPosition.Builder().target(center).zoom(10).build();
//        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

//        placeAutoComplete.setOnPlaceSelectedListener(new PlaceSelectionListener() {
//            @Override
//            public void onPlaceSelected(Place place) {
//
//                Log.d("Maps", "Place selected: " + place.getName());
//                String name = (String) place.getName();
//                LatLng latLng = place.getLatLng();
//
//                //move map camera
//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
//            }
//
//            @Override
//            public void onError(Status status) {
//                Log.d("Maps", "An error occurred: " + status);
//            }
//        });

//        mMap.addMarker(new MarkerOptions()
//                .position(center)
//                .title("Set Lokasi Usaha")
//                .snippet("Tekan lama dan geser marker")
//                .draggable(true)
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_lokasi)));

//        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
//            @Override
//            public void onMarkerDragStart(Marker marker) {
//                latitude = marker.getPosition().latitude;
//                longitude = marker.getPosition().longitude;
//
//                et_latitude.setText(String.valueOf(latitude));
//                et_longitude.setText(String.valueOf(longitude));
//            }
//
//            @Override
//            public void onMarkerDrag(Marker marker) {
//                latitude = marker.getPosition().latitude;
//                longitude = marker.getPosition().longitude;
//
//                et_latitude.setText(String.valueOf(latitude));
//                et_longitude.setText(String.valueOf(longitude));
//            }
//
//            @Override
//            public void onMarkerDragEnd(Marker marker) {
//                latitude = marker.getPosition().latitude;
//                longitude = marker.getPosition().longitude;
//
//                et_latitude.setText(String.valueOf(latitude));
//                et_longitude.setText(String.valueOf(longitude));
//            }
//        });
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setSmallestDisplacement(0.1F);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mCurrLocationMarker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("Set Lokasi Usaha")
                .snippet("Tekan lama dan geser marker")
                .draggable(true)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_lokasi)));

        et_latitude.setText(String.valueOf(location.getLatitude()));
        et_longitude.setText(String.valueOf(location.getLongitude()));

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                latitude = marker.getPosition().latitude;
                longitude = marker.getPosition().longitude;

                et_latitude.setText(String.valueOf(latitude));
                et_longitude.setText(String.valueOf(longitude));
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                latitude = marker.getPosition().latitude;
                longitude = marker.getPosition().longitude;

                et_latitude.setText(String.valueOf(latitude));
                et_longitude.setText(String.valueOf(longitude));
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                latitude = marker.getPosition().latitude;
                longitude = marker.getPosition().longitude;

                et_latitude.setText(String.valueOf(latitude));
                et_longitude.setText(String.valueOf(longitude));
            }
        });

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

        if (mGoogleApiClient != null)
        {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient != null &&
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    private void simpanData(){
        UserModel user = SharedPrefManager.getInstance(this).getUser();

        final String nama = et_nama.getText().toString().trim();
        final String deskripsi = et_deskripsi.getText().toString().trim();
        final String alamat = et_alamat.getText().toString().trim();
        final String telepon = et_telepon.getText().toString().trim();
        final String tahun = et_tahun.getText().toString().trim();
        final String pegPria = et_pria.getText().toString().trim();
        final String noIzinUsaha = et_noIzinUsaha.getText().toString().trim();
        final String pegWanita = et_wanita.getText().toString().trim();
        final String latitude = et_latitude.getText().toString().trim();
        final String longitude = et_longitude.getText().toString().trim();
        final String userId = String.valueOf(user.getId());

        if(TextUtils.isEmpty(nama)){
            et_nama.setError("Nama produk tidak boleh kosong"); // show error
            et_nama.requestFocus(); // hide error
            return;
        }

        if(TextUtils.isEmpty(deskripsi)){
            et_deskripsi.setError("Deskripsi produk tidak boleh kosong"); // show error
            et_deskripsi.requestFocus(); // hide error
            return;
        }

        if(TextUtils.isEmpty(alamat)){
            et_alamat.setError("Alamat tidak boleh kosong"); // show error
            et_alamat.requestFocus(); // hide error
            return;
        }

        if(TextUtils.isEmpty(telepon)){
            et_telepon.setError("Nomor telepon tidak boleh kosong"); // show error
            et_telepon.requestFocus(); // hide error
            return;
        }

        if(TextUtils.isEmpty(tahun)){
            et_tahun.setError("Tahun tidak boleh kosong"); // show error
            et_tahun.requestFocus(); // hide error
            return;
        }

        if(TextUtils.isEmpty(pegPria)){
            et_pria.setError("Jumlah pegawai pria tidak boleh kosong"); // show error
            et_pria.requestFocus(); // hide error
            return;
        }

        if(TextUtils.isEmpty(pegWanita)){
            et_wanita.setError("Jumlah pegawai wanita tidak boleh kosong"); // show error
            et_wanita.requestFocus(); // hide error
            return;
        }

//        if(TextUtils.isEmpty(noIzinUsaha)){
//            et_noIzinUsaha.setError("Nomor izin usaha tidak boleh kosong"); // show error
//            et_noIzinUsaha.requestFocus(); // hide error
//            return;
//        }

        if(TextUtils.isEmpty(latitude)){
            et_latitude.setError("Latitude tidak boleh kosong"); // show error
            et_latitude.requestFocus(); // hide error
            return;
        }

        if(TextUtils.isEmpty(longitude)){
            et_longitude.setError("Longitude tidak boleh kosong"); // show error
            et_longitude.requestFocus(); // hide error
            return;
        }

        final ProgressDialog progressDialog = ProgressDialog.show(this, "", "Menyimpan data usaha...", false, false);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_TAMBAH_USAHA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);

                            //if no error in response
                            if (obj.getBoolean("success")) {

                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                                finish();

                            } else {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("nama", nama);
                params.put("alamat", alamat);
                params.put("no_telp", telepon);
                params.put("tahun_data", tahun);
                params.put("latitude", latitude);
                params.put("longitude", longitude);
                params.put("peg_lk", pegPria);
                params.put("peg_pr", pegWanita);
                params.put("deskripsi", deskripsi);
                params.put("no_izin_usaha", noIzinUsaha);
                params.put("id_desa", desa);
                params.put("id_omset", omset);
                params.put("id_aset", aset);
                params.put("id_pelaku", userId);
                params.put("id_izin_usaha", izinUsaha);
                params.put("id_jenis_usaha", jenisUsaha);
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void getProvinsi(){

        if(provinsiModelList.size() > 0){
            if (kabupatenModelList.size() > 0) {
                if(kecamatanModelList.size() > 0){
                    if(desaModelList.size() > 0){
                        desaModelList.clear();
                        adapterDesa.notifyDataSetChanged();
                    }
                    kecamatanModelList.clear();
                    adapterKecamatan.notifyDataSetChanged();
                }
                kabupatenModelList.clear();
                adapterKabupaten.notifyDataSetChanged();
            }
            provinsiModelList.clear();
            adapterProvinsi.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_PROVINSI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length(); i++){
                                ProvinsiModel provinsiModel = new Gson().fromJson(array.getString(i), ProvinsiModel.class);
                                provinsiModelList.add(provinsiModel);
                            }

                            adapterProvinsi = new ArrayAdapter<>(TambahUsahaActivity.this, android.R.layout.simple_spinner_item, provinsiModelList);
                            adapterProvinsi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_provinsi.setAdapter(adapterProvinsi);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TambahUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getKabupaten(String provinsi){
        if (kabupatenModelList.size() > 0) {
            if(kecamatanModelList.size() > 0){
                if(desaModelList.size() > 0){
                    desaModelList.clear();
                    adapterDesa.notifyDataSetChanged();
                }
                kecamatanModelList.clear();
                adapterKecamatan.notifyDataSetChanged();
            }
            kabupatenModelList.clear();
            adapterKabupaten.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_KABUPATEN + provinsi,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length(); i++){
                                KabupatenModel kabupatenModel = new Gson().fromJson(array.getString(i), KabupatenModel.class);
                                kabupatenModelList.add(kabupatenModel);
                            }

                            adapterKabupaten = new ArrayAdapter<>(TambahUsahaActivity.this, android.R.layout.simple_spinner_item, kabupatenModelList);
                            adapterKabupaten.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_kabupaten.setAdapter(adapterKabupaten);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TambahUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getKecamatan(String kabupaten){
        if (kecamatanModelList.size() > 0) {
            if(desaModelList.size() > 0){
                desaModelList.clear();
                adapterDesa.notifyDataSetChanged();
            }
            kecamatanModelList.clear();
            adapterKecamatan.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_KECAMATAN + kabupaten,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length(); i++){
                                KecamatanModel kecamatanModel = new Gson().fromJson(array.getString(i), KecamatanModel.class);
                                kecamatanModelList.add(kecamatanModel);
                            }

                            adapterKecamatan = new ArrayAdapter<>(TambahUsahaActivity.this, android.R.layout.simple_spinner_item, kecamatanModelList);
                            adapterKecamatan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_kecamatan.setAdapter(adapterKecamatan);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TambahUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getDesa(String kecamatan){
        if (desaModelList.size() > 0) {
            desaModelList.clear();
            adapterDesa.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_DESA + kecamatan,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length(); i++){
                                DesaModel desaModel = new Gson().fromJson(array.getString(i), DesaModel.class);
                                desaModelList.add(desaModel);
                            }

                            adapterDesa = new ArrayAdapter<>(TambahUsahaActivity.this, android.R.layout.simple_spinner_item, desaModelList);
                            adapterDesa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_desa.setAdapter(adapterDesa);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TambahUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getSektorUsaha(){
        if (sektorUsahaModelList.size() > 0) {
            if(jenisUsahaModelList.size() > 0){
                jenisUsahaModelList.clear();
                adapterJenisUsaha.notifyDataSetChanged();
            }
            sektorUsahaModelList.clear();
            adapterSektorUsaha.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_SEKTOR_USAHA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length(); i++){
                                SektorUsahaModel sektorUsahaModel = new Gson().fromJson(array.getString(i), SektorUsahaModel.class);
                                sektorUsahaModelList.add(sektorUsahaModel);
                            }

                            adapterSektorUsaha = new ArrayAdapter<>(TambahUsahaActivity.this, android.R.layout.simple_spinner_item, sektorUsahaModelList);
                            adapterSektorUsaha.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_sektor.setAdapter(adapterSektorUsaha);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TambahUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);
    }

    private void getJenisUsaha(String sektor){
        if (jenisUsahaModelList.size() > 0) {
            jenisUsahaModelList.clear();
            adapterJenisUsaha.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_JENIS_USAHA + sektor,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length(); i++){
                                JenisUsahaModel jenisUsahaModel = new Gson().fromJson(array.getString(i), JenisUsahaModel.class);
                                jenisUsahaModelList.add(jenisUsahaModel);
                            }

                            adapterJenisUsaha = new ArrayAdapter<>(TambahUsahaActivity.this, android.R.layout.simple_spinner_item, jenisUsahaModelList);
                            adapterJenisUsaha.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_jenis.setAdapter(adapterJenisUsaha);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TambahUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getOmset(){

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_OMSET,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length(); i++){
                                OmsetModel omsetModel = new Gson().fromJson(array.getString(i), OmsetModel.class);
                                omsetModelList.add(omsetModel);
                            }

                            adapterOmset = new ArrayAdapter<>(TambahUsahaActivity.this, android.R.layout.simple_spinner_item, omsetModelList);
                            adapterOmset.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_omset.setAdapter(adapterOmset);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TambahUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getAset(){

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_ASET,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length(); i++){
                                AsetModel asetModel = new Gson().fromJson(array.getString(i), AsetModel.class);
                                asetModelList.add(asetModel);
                            }

                            adapterAset = new ArrayAdapter<>(TambahUsahaActivity.this, android.R.layout.simple_spinner_item, asetModelList);
                            adapterAset.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_aset.setAdapter(adapterAset);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TambahUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getIzinUsaha(){

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_IZIN_USAHA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length(); i++){
                                IzinUsahaModel izinUsahaModel = new Gson().fromJson(array.getString(i), IzinUsahaModel.class);
                                izinUsahaModelList.add(izinUsahaModel);
                            }

                            adapterIzinUsaha = new ArrayAdapter<>(TambahUsahaActivity.this, android.R.layout.simple_spinner_item, izinUsahaModelList);
                            adapterIzinUsaha.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_izin.setAdapter(adapterIzinUsaha);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TambahUsahaActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }


    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
