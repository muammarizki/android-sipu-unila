package com.ilkom.mrfi.petaumkm;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.ilkom.mrfi.petaumkm.helpers.BottomNavigationViewHelper;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.models.UserModel;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            UserModel user = SharedPrefManager.getInstance(getApplicationContext()).getUser();
            switch (item.getItemId()) {
                case R.id.menu_home:
                    FragmentHome fragmentHome= new FragmentHome();
                    FragmentTransaction fragmentTransactionHome = getSupportFragmentManager().beginTransaction();
                    fragmentTransactionHome.replace(R.id.container, fragmentHome);
                    fragmentTransactionHome.commit();
                    return true;

                case R.id.menu_wishlist:
                    FragmentWishlist fragmentWishlist = new FragmentWishlist();
                    FragmentTransaction fragmentTransactionWishlist = getSupportFragmentManager().beginTransaction();
                    fragmentTransactionWishlist.replace(R.id.container, fragmentWishlist );
                    fragmentTransactionWishlist.commit();
                    return true;

                case R.id.menu_map:
                    FragmentMap fragmentMap = new FragmentMap();
                    FragmentTransaction fragmentTransactionMap = getSupportFragmentManager().beginTransaction();
                    fragmentTransactionMap.replace(R.id.container, fragmentMap);
                    fragmentTransactionMap.commit();
                    return true;

                case R.id.menu_profile:
                    FragmentProfile fragmentProfile = new FragmentProfile();
                    FragmentTransaction fragmentTransactionProfile = getSupportFragmentManager().beginTransaction();
                    fragmentTransactionProfile.replace(R.id.container, fragmentProfile);
                    fragmentTransactionProfile.commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        FragmentHome fragmentHome = new FragmentHome();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragmentHome);
        fragmentTransaction.commit();

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        BottomNavigationViewHelper.removeShiftMode(navigation);

    }

    public void setActionBarTitle(String title){
        getSupportActionBar().setTitle(title);
    }
}
