package com.ilkom.mrfi.petaumkm;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.helpers.VolleySingleton;
import com.ilkom.mrfi.petaumkm.models.DesaModel;
import com.ilkom.mrfi.petaumkm.models.KabupatenModel;
import com.ilkom.mrfi.petaumkm.models.KecamatanModel;
import com.ilkom.mrfi.petaumkm.models.ProvinsiModel;
import com.ilkom.mrfi.petaumkm.models.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TambahBiodataActivity extends AppCompatActivity {

    ArrayAdapter<ProvinsiModel> adapterProvinsi;
    ArrayAdapter<KabupatenModel> adapterKabupaten;
    ArrayAdapter<KecamatanModel> adapterKecamatan;
    ArrayAdapter<DesaModel> adapterDesa;

    private EditText et_npwp, et_ktp, et_alamat;
    private Spinner sp_provinsi, sp_kabupaten, sp_kecamatan, sp_desa;

    private List<ProvinsiModel> provinsiModelList = new ArrayList<>();
    private List<KabupatenModel> kabupatenModelList = new ArrayList<>();
    private List<KecamatanModel> kecamatanModelList = new ArrayList<>();
    private List<DesaModel> desaModelList = new ArrayList<>();

    String id_desa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biodata_pelaku);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        sp_provinsi = findViewById(R.id.sp_provinsi);
        getProvinsi();

        sp_provinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String provinsi = String.valueOf(provinsiModelList.get(position).getProvinsiId());
                getKabupaten(provinsi);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_kabupaten = findViewById(R.id.sp_kabupaten);
        sp_kabupaten.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String kabupaten = String.valueOf(kabupatenModelList.get(position).getKabupatenId());
                getKecamatan(kabupaten);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_kecamatan = findViewById(R.id.sp_kecamatan);
        sp_kecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String kecamatan = String.valueOf(kecamatanModelList.get(position).getKecamatanId());
                getDesa(kecamatan);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_desa = findViewById(R.id.sp_desa);
        sp_desa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                id_desa = String.valueOf(desaModelList.get(position).getDesaId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        et_npwp = findViewById(R.id.et_npwp);
        et_ktp = findViewById(R.id.et_ktp);
        et_alamat = findViewById(R.id.et_alamat);
    }

    private void simpanData(){
        UserModel user = SharedPrefManager.getInstance(this).getUser();

        final String npwp = et_npwp.getText().toString().trim();
        final String ktp = et_ktp.getText().toString().trim();
        final String alamat = et_alamat.getText().toString().trim();
        final String id_user = String.valueOf(user.getId());

//        if(TextUtils.isEmpty(npwp)){
//            et_npwp.setError("NPWP tidak boleh kosong"); // show error
//            et_npwp.requestFocus(); // hide error
//            return;
//        }
//
//        if(TextUtils.isEmpty(ktp)){
//            et_ktp.setError("Nomor KTP tidak boleh kosong"); // show error
//            et_ktp.requestFocus(); // hide error
//            return;
//        }

        if(TextUtils.isEmpty(alamat)){
            et_alamat.setError("Alamat tidak boleh kosong"); // show error
            et_alamat.requestFocus(); // hide error
            return;
        }

        final ProgressDialog progressDialog = ProgressDialog.show(this, "", "Menyimpan data diri...", false, false);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_TAMBAH_BIODATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);

                            //if no error in response
                            if (obj.getBoolean("success")) {
                               
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                finish();

                            } else {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        progressDialog.dismiss();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(TambahBiodataActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("npwp", npwp);
                params.put("no_ktp", ktp);
                params.put("alamat", alamat);
                params.put("id_user", id_user);
                params.put("id_desa", id_desa);
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void getProvinsi(){

        if(provinsiModelList.size() > 0){
            if (kabupatenModelList.size() > 0) {
                if(kecamatanModelList.size() > 0){
                    if(desaModelList.size() > 0){
                        desaModelList.clear();
                        adapterDesa.notifyDataSetChanged();
                    }
                    kecamatanModelList.clear();
                    adapterKecamatan.notifyDataSetChanged();
                }
                kabupatenModelList.clear();
                adapterKabupaten.notifyDataSetChanged();
            }
            provinsiModelList.clear();
            adapterProvinsi.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_PROVINSI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length(); i++){
                                ProvinsiModel provinsiModel = new Gson().fromJson(array.getString(i), ProvinsiModel.class);
                                provinsiModelList.add(provinsiModel);
                            }

                            adapterProvinsi = new ArrayAdapter<>(TambahBiodataActivity.this, android.R.layout.simple_spinner_item, provinsiModelList);
                            adapterProvinsi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_provinsi.setAdapter(adapterProvinsi);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TambahBiodataActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getKabupaten(String provinsi){
        if (kabupatenModelList.size() > 0) {
            if(kecamatanModelList.size() > 0){
                if(desaModelList.size() > 0){
                    desaModelList.clear();
                    adapterDesa.notifyDataSetChanged();
                }
                kecamatanModelList.clear();
                adapterKecamatan.notifyDataSetChanged();
            }
            kabupatenModelList.clear();
            adapterKabupaten.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_KABUPATEN + provinsi,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length(); i++){
                                KabupatenModel kabupatenModel = new Gson().fromJson(array.getString(i), KabupatenModel.class);
                                kabupatenModelList.add(kabupatenModel);
                            }

                            adapterKabupaten = new ArrayAdapter<>(TambahBiodataActivity.this, android.R.layout.simple_spinner_item, kabupatenModelList);
                            adapterKabupaten.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_kabupaten.setAdapter(adapterKabupaten);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TambahBiodataActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getKecamatan(String kabupaten){
        if (kecamatanModelList.size() > 0) {
            if(desaModelList.size() > 0){
                desaModelList.clear();
                adapterDesa.notifyDataSetChanged();
            }
            kecamatanModelList.clear();
            adapterKecamatan.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_KECAMATAN + kabupaten,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length(); i++){
                                KecamatanModel kecamatanModel = new Gson().fromJson(array.getString(i), KecamatanModel.class);
                                kecamatanModelList.add(kecamatanModel);
                            }

                            adapterKecamatan = new ArrayAdapter<>(TambahBiodataActivity.this, android.R.layout.simple_spinner_item, kecamatanModelList);
                            adapterKecamatan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_kecamatan.setAdapter(adapterKecamatan);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TambahBiodataActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void getDesa(String kecamatan){
        if (desaModelList.size() > 0) {
            desaModelList.clear();
            adapterDesa.notifyDataSetChanged();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLs.URL_DESA + kecamatan,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for(int i = 0;i < array.length(); i++){
                                DesaModel desaModel = new Gson().fromJson(array.getString(i), DesaModel.class);
                                desaModelList.add(desaModel);
                            }

                            adapterDesa = new ArrayAdapter<>(TambahBiodataActivity.this, android.R.layout.simple_spinner_item, desaModelList);
                            adapterDesa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            sp_desa.setAdapter(adapterDesa);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(TambahBiodataActivity.this, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(this).add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.simpan, menu);
        MenuItem item = menu.findItem(R.id.menu_simpan);
        item.setIcon(R.drawable.ic_simpan).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case  R.id.menu_simpan:
                simpanData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

}