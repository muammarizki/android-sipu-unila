package com.ilkom.mrfi.petaumkm.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by muamm on 1/27/2018.
 */

public class OmsetModel {

    @SerializedName("id")
    private int omsetId;

    @SerializedName("omset_bawah")
    private String batasBawahOmset;

    @SerializedName("omset_atas")
    private String batasAtasOmset;

    @SerializedName("nama")
    private String namaOmset;

    public int getOmsetId() {
        return omsetId;
    }

    public void setOmsetId(int omsetId) {
        this.omsetId = omsetId;
    }

    public String getBatasBawahOmset() {
        return batasBawahOmset;
    }

    public void setBatasBawahOmset(String batasBawahOmset) {
        this.batasBawahOmset = batasBawahOmset;
    }

    public String getBatasAtasOmset() {
        return batasAtasOmset;
    }

    public void setBatasAtasOmset(String batasAtasOmset) {
        this.batasAtasOmset = batasAtasOmset;
    }

    public String getNamaOmset() {
        return namaOmset;
    }

    public void setNamaOmset(String namaOmset) {
        this.namaOmset = namaOmset;
    }

    @Override
    public String toString(){

        return batasBawahOmset + " s.d " + batasAtasOmset;
    }
}
