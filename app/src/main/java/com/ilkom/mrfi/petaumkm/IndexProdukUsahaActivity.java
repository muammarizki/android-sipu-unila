package com.ilkom.mrfi.petaumkm;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.LoginActivity;
import com.ilkom.mrfi.petaumkm.R;
import com.ilkom.mrfi.petaumkm.adapters.ProdukBaruAdapter;
import com.ilkom.mrfi.petaumkm.adapters.ProdukIndexGridAdapter;
import com.ilkom.mrfi.petaumkm.helpers.SharedPrefManager;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.ProdukBaruModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by muamm on 3/28/2018.
 */

public class IndexProdukUsahaActivity extends AppCompatActivity {

    int id_usaha;
    String nama;
    List<ProdukBaruModel> produkModels;
    ProdukIndexGridAdapter produkIndexGridAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView rv_produk;
    LinearLayout errorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index_produk);
        setTitle("Produk " + getIntent().getStringExtra("usaha"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        id_usaha = getIntent().getIntExtra("id_usaha", id_usaha);

        int columns = 2;
        rv_produk = findViewById(R.id.rv_produk);
        rv_produk.setLayoutManager(new GridLayoutManager(this, columns));
        produkModels = new ArrayList<>();
        produkIndexGridAdapter = new ProdukIndexGridAdapter(IndexProdukUsahaActivity.this);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                produkModels.clear();
                getProduk();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        getProduk();
    }

    private void getProduk() {
        if (produkModels.size() > 0) {
            produkModels.clear();
            produkIndexGridAdapter.clearModel();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_GET_PRODUK_USAHA_LIMIT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("message");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject o = array.getJSONObject(i);
                                produkModels.add(new ProdukBaruModel(
                                        o.getInt("id"),
                                        o.getString("nama"),
                                        o.getString("harga"),
                                        o.getString("thumbnail"),
                                        o.getInt("pengunjung"),
                                        o.getString("usaha"),
                                        o.getString("kota")
                                ));
                            }

                            produkIndexGridAdapter.setProdukModels(produkModels);
                            rv_produk.setAdapter(produkIndexGridAdapter);

//                            if(produkBaruAdapter.getItemCount() == 0){
//                                rv_produk.setVisibility(View.GONE);
//                                emptyProduk.setVisibility(View.VISIBLE);
//                            }else{
//                                rv_produk.setVisibility(View.VISIBLE);
//                                emptyProduk.setVisibility(View.GONE);
//                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_usaha", String.valueOf(id_usaha));
                return params;
            }
        };
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(getApplicationContext()).add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Cari Produk...");
        searchView.setIconifiedByDefault(false);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                final List<ProdukBaruModel> filteredModelList = filter(produkModels, s);

                produkIndexGridAdapter.setFilter(filteredModelList);
                return false;
            }
        });

        return true;
    }

    private List<ProdukBaruModel> filter(List<ProdukBaruModel> models, String query) {
        query = query.toLowerCase();final List<ProdukBaruModel> filteredModelList = new ArrayList<>();
        for (ProdukBaruModel model : models) {
            final String text = model.getNama().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
