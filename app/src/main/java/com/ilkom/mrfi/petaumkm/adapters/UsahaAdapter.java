package com.ilkom.mrfi.petaumkm.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ilkom.mrfi.petaumkm.*;
import com.ilkom.mrfi.petaumkm.helpers.URLs;
import com.ilkom.mrfi.petaumkm.models.UsahaModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by muamm on 1/21/2018.
 */

public class UsahaAdapter extends RecyclerView.Adapter<UsahaAdapter.UsahaViewHolder> {
    private Context context;
    private List<UsahaModel> usahaModels;

    public UsahaAdapter(Context context){
        this.context = context;
        this.usahaModels = new ArrayList<>();
    }

    public UsahaAdapter.UsahaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.usaha_item, null);
        return new UsahaAdapter.UsahaViewHolder(view);
    }

    public void setFilter(List<UsahaModel> usahaModel) {
        usahaModels = new ArrayList<>();
        usahaModels.addAll(usahaModel);
        notifyDataSetChanged();
    }

    public void setUsahaModels(List<UsahaModel> usahaModels) {
        this.usahaModels = usahaModels;
        notifyDataSetChanged();
    }

    public void clearModel(){
        usahaModels.clear();
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final UsahaAdapter.UsahaViewHolder holder, final int position) {
         final UsahaModel usaha = usahaModels.get(position);

        holder.tv_nama.setText(usaha.getNama());
        holder.tv_deskripsi.setText(usaha.getDeskripsi());

        holder.btn_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating a popup menu
                final PopupMenu popup = new PopupMenu(context, holder.btn_option);
                //inflating menu from xml resource
                popup.inflate(R.menu.usaha_options);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.lihat:
                                Intent intent = new Intent(context, DetailUsahaActivity.class);
                                intent.putExtra("id", usahaModels.get(position).getId());
                                context.startActivity(intent);
                                break;
                            case R.id.edit:
                                Intent i = new Intent(context, EditUsahaActivity.class);
                                i.putExtra("id", usahaModels.get(position).getId());
                                i.putExtra("nama", usahaModels.get(position).getNama());
                                context.startActivity(i);
                                break;
                            case R.id.hapus:
                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setTitle("Konfirmasi");
                                builder.setMessage("Apakah anda yakin ingin menghapus\n" +
                                        usahaModels.get(position).getNama() + " ?");
                                builder.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        hapusData(usahaModels.get(position).getId());
                                        usahaModels.remove(position);
                                        notifyDataSetChanged();
                                        dialog.dismiss();
                                    }
                                });
                                builder.setNegativeButton("Batalkan", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                AlertDialog alert = builder.create();
                                alert.show();
                                break;
                        }

                        return false;
                    }
                });
                //displaying the popup
                popup.show();
            }
        });
    }

    private void hapusData(final int id){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_HAPUS_USAHA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);

                            if(obj.getBoolean("success")){
                                Toast.makeText(context, obj.getString("message"),Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(context, obj.getString("message"),Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id_usaha", String.valueOf(id));
                return params;
            }
        };
        //The following lines add the request to the volley queue
        //These are very important
        Volley.newRequestQueue(context).add(stringRequest);
    }

    @Override
    public int getItemCount() {
//        return usahaModels.size();
        return usahaModels == null ? 0 : usahaModels.size();
    }

    class UsahaViewHolder extends RecyclerView.ViewHolder {

        TextView tv_nama, tv_deskripsi;
        ImageView btn_option;

        public UsahaViewHolder(View itemView) {
            super(itemView);

            tv_nama = itemView.findViewById(R.id.tv_nama);
            tv_deskripsi = itemView.findViewById(R.id.tv_deskripsi);
            btn_option = itemView.findViewById(R.id.textViewOptions);
        }
    }

}
