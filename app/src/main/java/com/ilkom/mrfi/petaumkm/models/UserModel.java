package com.ilkom.mrfi.petaumkm.models;


/**
 * Created by muamm on 12/27/2017.
 */

public class UserModel {
    private int id;
    private int peran;
    private String username;

    public UserModel(int id, int peran, String username) {
        this.id = id;
        this.peran = peran;
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPeran() {
        return peran;
    }

    public void setPeran(int peran) {
        this.peran = peran;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
