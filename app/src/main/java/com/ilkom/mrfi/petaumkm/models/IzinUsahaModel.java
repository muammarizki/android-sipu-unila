package com.ilkom.mrfi.petaumkm.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by muamm on 1/27/2018.
 */

public class IzinUsahaModel {

    @SerializedName("id")
    private int izinUsahaId;

    @SerializedName("nama")
    private String namaIzinUsaha;

    public int getIzinUsahaId() {
        return izinUsahaId;
    }

    public void setIzinUsahaId(int izinUsahaId) {
        this.izinUsahaId = izinUsahaId;
    }

    public String getNamaIzinUsaha() {
        return namaIzinUsaha;
    }

    public void setNamaIzinUsaha(String namaIzinUsaha) {
        this.namaIzinUsaha = namaIzinUsaha;
    }

    @Override
    public String toString(){
        return namaIzinUsaha;
    }
}
