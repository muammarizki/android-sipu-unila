package com.ilkom.mrfi.petaumkm.models;

/**
 * Created by muamm on 2/7/2018.
 */

public class UsahaLimitModel {

    int id;
    String nama;
    String kota;
    float rating;
    int jumlah;

    public UsahaLimitModel(int id, String nama, String kota, float rating, int jumlah) {
        this.id = id;
        this.nama = nama;
        this.kota = kota;
        this.rating = rating;
        this.jumlah = jumlah;
    }

    public String getKota() {
        return kota;
    }

    public float getRating() {
        return rating;
    }

    public int getJumlah() {
        return jumlah;
    }

    public int getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }
}
